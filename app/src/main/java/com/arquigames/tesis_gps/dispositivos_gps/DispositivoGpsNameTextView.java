package com.arquigames.tesis_gps.dispositivos_gps;

import android.content.Context;
import android.util.AttributeSet;

public class DispositivoGpsNameTextView extends android.support.v7.widget.AppCompatTextView {
    private String dispositivo_gps_id;
    public DispositivoGpsNameTextView(Context context) {
        super(context);
    }

    public DispositivoGpsNameTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DispositivoGpsNameTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public String getDispositivo_gps_id() {
        return dispositivo_gps_id;
    }

    public void setDispositivo_gps_id(String dispositivo_gps_id) {
        this.dispositivo_gps_id = dispositivo_gps_id;
    }
}
