package com.arquigames.tesis_gps.dispositivos_gps;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.arquigames.tesis_gps.R;
import com.arquigames.tesis_gps.webservices.WS_DispositivosGps;

import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DispositivoGpsFragmentForm.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DispositivoGpsFragmentForm#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DispositivoGpsFragmentForm extends AppCompatDialogFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "DispositivoGpsFragmentForm";
    private static String ARG_NAME = "name";
    private static String ARG_EMAIL = "email";

    // TODO: Rename and change types of parameters
    private EditText text_brand;
    private EditText text_model;
    private EditText text_imei;
    private EditText text_simcard;
    private Spinner spinner_operator;
    private EditText text_phone_number;
    private EditText text_description;
    private TextView textview_wait_message;

    private Button btn_go;
    private Button btn_cancel;

    private OnFragmentInteractionListener mListener;
    private Bundle bundle;
    private TextView text_title;

    public DispositivoGpsFragmentForm() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DispositivoGpsFragmentForm.
     */
    // TODO: Rename and change types and number of parameters
    public static DispositivoGpsFragmentForm newInstance(String param1, String param2) {
        DispositivoGpsFragmentForm fragment = new DispositivoGpsFragmentForm();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View root = inflater.inflate(R.layout.dialog_fragment_dispositivos_gps_form, null);
        builder.setView(root);


        bundle = getArguments();
        text_title          = root.findViewById(R.id.dispositivos_gps_dialog_title);
        text_brand          = root.findViewById(R.id.dispositivos_gps_dialog_brand);
        text_model          = root.findViewById(R.id.dispositivos_gps_dialog_model);
        text_imei           = root.findViewById(R.id.dispositivos_gps_dialog_imei);
        text_simcard        = root.findViewById(R.id.dispositivos_gps_dialog_simcard);
        spinner_operator    = root.findViewById(R.id.dispositivos_gps_dialog_operator);
        text_phone_number   = root.findViewById(R.id.dispositivos_gps_dialog_phone_number);
        text_description    = root.findViewById(R.id.dispositivos_gps_dialog_description);
        textview_wait_message = root.findViewById(R.id.dispositivos_gps_dialog_wait_message);
        this.hideWaitMessage();

        text_brand.setText(bundle.getString("brand"));
        text_model.setText(bundle.getString("model"));
        text_imei.setText(bundle.getString("imei"));
        text_simcard.setText(bundle.getString("simcard"));
        this.setSelectedOperator(spinner_operator,bundle.getString("operator"));
        text_phone_number.setText(bundle.getString("phone_number"));
        text_description.setText(bundle.getString("description"));
        btn_go      = root.findViewById(R.id.dispositivos_gps_dialog_go);
        btn_cancel  = root.findViewById(R.id.dispositivos_gps_dialog_cancel);
        final int action;
        action = bundle.getInt("new",1);
        if(action==1){
            text_title.setText("Nuevo dispositivo gps");
            btn_go.setText(R.string.register);
        }else{
            text_title.setText("Detalles de dispositivo gps");
            btn_go.setText(R.string.update);
        }
        btn_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DispositivoGpsFragmentForm.this.waitMessage(1);

                HashMap<String,String> data = new HashMap<String,String>();
                data.put("brand",text_brand.getText().toString());
                data.put("model",text_model.getText().toString());
                data.put("imei",text_imei.getText().toString());
                data.put("simcard",text_simcard.getText().toString());
                data.put("operator",spinner_operator.getSelectedItem().toString());
                data.put("phone_number",text_phone_number.getText().toString());
                data.put("description",text_description.getText().toString());
                data.put("id",bundle.getString("id"));

                WS_DispositivosGps.insert_or_update(DispositivoGpsFragmentForm.this,data);
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DispositivoGpsFragmentForm.this.getDialog().cancel();
            }
        });



        if(savedInstanceState!=null){
            String brand = savedInstanceState.getString("brand","");
            String model = savedInstanceState.getString("model","");
            String imei = savedInstanceState.getString("imei","");
            String operator= savedInstanceState.getString("operator","");
            String simcard= savedInstanceState.getString("simcard","");
            String phone_number= savedInstanceState.getString("phone_number","");
            String description = savedInstanceState.getString("description","");
            if(!brand.equals(""))text_brand.setText(brand);
            if(!model.equals(""))text_model.setText(model);
            if(!imei.equals(""))text_imei.setText(imei);
            if(!operator.equals(""))this.setSelectedOperator(spinner_operator,operator);
            if(!simcard.equals(""))text_simcard.setText(simcard);
            if(!phone_number.equals(""))text_phone_number.setText(phone_number);
            if(!description.equals(""))text_description.setText(description);
        }

        return builder.create();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(text_brand!=null && text_brand.getText()!=null){
            outState.putString("brand", text_brand.getText().toString());
        }
        if(text_model!=null && text_model.getText()!=null){
            outState.putString("model", text_model.getText().toString());
        }
        if(text_imei!=null && text_imei.getText()!=null){
            outState.putString("imei", text_imei.getText().toString());
        }
        if(text_simcard!=null && text_simcard.getText()!=null){
            outState.putString("simcard", text_simcard.getText().toString());
        }
        if(text_phone_number!=null && text_phone_number.getText()!=null){
            outState.putString("phone_number", text_phone_number.getText().toString());
        }
        if(text_description!=null && text_description.getText()!=null){
            outState.putString("description", text_description.getText().toString());
        }
        if(spinner_operator!=null && spinner_operator.getSelectedItem().toString()!=null){
            outState.putString("operator", spinner_operator.getSelectedItem().toString());
        }
    }

    private void waitMessage(int state) {
        textview_wait_message.setText(R.string.wait_please);
        textview_wait_message.setVisibility(View.VISIBLE);
    }
    private void hideWaitMessage() {
        textview_wait_message.setVisibility(View.GONE);
    }
    public void queryStatus(String status){
        textview_wait_message.setText(status);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = getArguments();
        if (bundle != null) {
            ARG_NAME = getArguments().getString(ARG_NAME);
            ARG_EMAIL = getArguments().getString(ARG_EMAIL);
        }
    }
    @Override
    public void onResume() {

        super.onResume();
    }
    private void setSelectedOperator(Spinner spinner, String value) {
        ArrayAdapter adapter = (ArrayAdapter) spinner.getAdapter();
        for (int position = 0; position < adapter.getCount(); position++) {
            if(adapter.getItem(position).toString().equals(value)) {
                spinner.setSelection(position);
                return;
            }
        }
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
