package com.arquigames.tesis_gps.dispositivos_gps;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.arquigames.tesis_gps.R;
import com.arquigames.tesis_gps.webservices.WS_DispositivosGps;

public class DispositivoGpsFragmentDelete extends AppCompatDialogFragment {
    private AppCompatTextView name;
    private AppCompatButton btn_delete;
    private AppCompatButton btn_cancel;
    private TextView textview_wait_message;
    private Bundle bundle;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        android.support.v7.app.AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.dialog_fragment_productos_delete, null));
        return builder.create();
    }
    @Override
    public void onResume() {

        super.onResume();
        bundle = getArguments();
        name                    = this.getDialog().findViewById(R.id.productos_dialog_name);
        btn_delete              = this.getDialog().findViewById(R.id.productos_dialog_delete);
        btn_cancel              = this.getDialog().findViewById(R.id.productos_dialog_cancel);
        textview_wait_message   = this.getDialog().findViewById(R.id.productos_dialog_delete_wait_message);
        name.setText(
                String.format("Desea eliminar el dispositivo 'gps %s %s / %s' ?",
                        bundle.getString("brand"),
                        bundle.getString("model"),
                        bundle.getString("phone_number")
                )
        );
        this.hideWaitMessage();

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DispositivoGpsFragmentDelete.this.getDialog().cancel();
            }
        });
        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DispositivoGpsFragmentDelete.this.waitMessage();
                WS_DispositivosGps.delete(DispositivoGpsFragmentDelete.this,bundle.getString("id"));

            }
        });
    }


    private void waitMessage() {
        textview_wait_message.setText(R.string.wait_please);
        textview_wait_message.setVisibility(View.VISIBLE);
    }
    private void hideWaitMessage() {
        textview_wait_message.setVisibility(View.GONE);
    }

    public void queryStatus(String status) {
        textview_wait_message.setText(status);
    }
}
