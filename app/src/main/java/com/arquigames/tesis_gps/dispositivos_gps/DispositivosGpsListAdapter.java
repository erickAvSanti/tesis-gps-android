package com.arquigames.tesis_gps.dispositivos_gps;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.arquigames.tesis_gps.LoginActivity;
import com.arquigames.tesis_gps.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

public class DispositivosGpsListAdapter extends ArrayAdapter<JSONObject> {
    private static final String TAG = "UsuariosListAdapter";
    private DispositivosGpsActivity ctx;
    private int resource;
    private HashMap<String, JSONObject> mapObjects = new HashMap<>();

    public DispositivosGpsListAdapter(DispositivosGpsActivity context, int textViewResourceId,
                                      List<JSONObject> objects) {
        super(context, textViewResourceId, objects);
        ctx = context;
        resource = textViewResourceId;
        for (int i = 0; i < objects.size(); ++i) {
            JSONObject obj = objects.get(i);
            try {
                String id = obj.getString("id");
                Log.e("Init ID",id);
                mapObjects.put(id,obj);
            } catch (JSONException e) {
                Log.e(TAG, LoginActivity.getStackTrace(e));
            }
        }
    }

    @Override
    public long getItemId(int position) {
        JSONObject item = getItem(position);
        int id = 0;
        try {
            id = Integer.parseInt(item.getString("id"));
            Log.e("getITemId",String.valueOf(id));
        } catch (JSONException e) {
            Log.e(TAG,LoginActivity.getStackTrace(e));
        }
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(ctx);
            v = vi.inflate(resource, null);
        }

        JSONObject data = getItem(position);

        if (data != null) {
            DispositivoGpsNameTextView name = v.findViewById(R.id.dispositivos_gps_list_name);
            DispositivoGpsImageButton close = v.findViewById(R.id.dispositivos_gps_list_delete);
            try {
                name.setText(
                        String.format("%s %s / %s",
                                data.getString("brand"),
                                data.getString("model"),
                                data.getString("phone_number")
                        )
                );
                name.setDispositivo_gps_id(data.getString("id"));
                close.setDispositivo_gps_delete_id(data.getString("id"));
                name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ctx.editDispositivoGps(((DispositivoGpsNameTextView)v).getDispositivo_gps_id());
                    }
                });
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ctx.deleteDispositivoGps(((DispositivoGpsImageButton)v).getDispositivo_gps_delete_id());
                    }
                });
            } catch (JSONException e) {
                Log.e(TAG,LoginActivity.getStackTrace(e));
                name.setText("Error");
            }
        }

        return v;
    }


    @Override
    public boolean hasStableIds() {
        return true;
    }
}
