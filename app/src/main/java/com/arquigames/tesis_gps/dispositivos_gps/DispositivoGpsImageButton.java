package com.arquigames.tesis_gps.dispositivos_gps;

import android.content.Context;
import android.support.v7.widget.AppCompatImageButton;
import android.util.AttributeSet;

public class DispositivoGpsImageButton extends AppCompatImageButton {
    private String dispositivo_gps_delete_id;
    public DispositivoGpsImageButton(Context context) {
        super(context);
    }

    public DispositivoGpsImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DispositivoGpsImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public String getDispositivo_gps_delete_id() {
        return dispositivo_gps_delete_id;
    }

    public void setDispositivo_gps_delete_id(String dispositivo_gps_delete_id) {
        this.dispositivo_gps_delete_id = dispositivo_gps_delete_id;
    }
}
