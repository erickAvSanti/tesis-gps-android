package com.arquigames.tesis_gps.webservices;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.arquigames.tesis_gps.AppActivity;
import com.arquigames.tesis_gps.LoginActivity;
import com.arquigames.tesis_gps.services.PingService;

import org.json.JSONObject;

import java.io.DataOutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class WS_Tracking{
    public static boolean try_reload_list              = true;
    public static String TAG = "WS_Tracking";
    public static boolean try_reload_list_for_service = true;

    public static void insertWithSocker(final AppActivity appActivity, final JSONObject data) {
        SocketTask socketTask = new SocketTask();
        socketTask.execute(data.toString());
    }

    public static void list(final AppActivity ctx, final String date) {
        if(!try_reload_list){
            return;
        }


        final Handler handler2 = new Handler(Looper.getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
                if(msg.obj!=null){
                    JSONObject json = (JSONObject) msg.obj;
                    ctx.renderMapPoints(json);
                }else{
                    ctx.renderMapPoints(null);
                }
                //Log.e(TAG, String.format("handleMessage = %s", json.toString()));
            }
        };


        HashMap<String,String> json = LoginActivity.getUserInfo(ctx);
        if(json!=null){
            try{
                final String token = json.containsKey("token") ? json.get("token") : "";

                //Log.e(AppActivity.TAG, "list WS_Tracking");
                try_reload_list = false;
                //showProgress(false);
                RequestQueue queue = Volley.newRequestQueue(ctx);

                StringRequest objRequest = new StringRequest(Request.Method.POST, AppActivity.URL_TRACKING, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try_reload_list = true;
                        //showProgress(false);
                        //Log.e(TAG, String.format("data => %s", response));
                        try {
                            JSONObject json = new JSONObject(response);
                            JSONObject data = json.getJSONObject("data");
                            Message message = handler2.obtainMessage(1,data);
                            message.sendToTarget();
                        } catch (Exception e) {
                            //Log.e(TAG,LoginActivity.getStackTrace(e));
                            Message message = handler2.obtainMessage(1,null);
                            message.sendToTarget();
                        }
                        LoginActivity.putSharedStringValue(ctx,AppActivity.KEY_SHARED_GPS_TRACKING,response);

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try_reload_list = true;
                        String msg = error.getMessage();
                        if(msg==null && error.networkResponse!=null)msg = String.format(Locale.US,"error %d", error.networkResponse.statusCode);
                        Log.e(TAG,msg);
                    }
                }){
                    @Override
                    protected Map<String,String> getParams(){
                        HashMap<String, String> params = new HashMap<String, String>();
                        params.put("date",date);
                        return params;
                    }
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String>  params = new HashMap<String, String>();
                        params.put("Content-Type", "application/x-www-form-urlencoded");
                        params.put("Accept", "application/json");
                        params.put("Authorization", String.format("Bearer %s", token));

                        return params;
                    }
                };
                queue.add(objRequest);
            }catch(Exception e){
                Log.e(TAG,LoginActivity.getStackTrace(e));
            }
        }
    }

    public static void listForService(final PingService ctx, final String date) {
        if(!try_reload_list_for_service){
            return;
        }


        final Handler handler2 = new Handler(ctx.getServiceLooper()){
            @Override
            public void handleMessage(Message msg) {
                if(msg.obj!=null){
                    JSONObject json = (JSONObject) msg.obj;
                    ctx.setContent(json);
                }else{
                    ctx.setContent(null);
                }
                //Log.e(TAG, String.format("handleMessage = %s", json.toString()));
            }
        };


        HashMap<String,String> json = LoginActivity.getUserInfo(ctx);
        if(json!=null){
            try{
                final String token = json.containsKey("token") ? json.get("token") : "";

                //Log.e(AppActivity.TAG, "list WS_Tracking");
                try_reload_list_for_service = false;
                //showProgress(false);
                RequestQueue queue = Volley.newRequestQueue(ctx);

                StringRequest objRequest = new StringRequest(Request.Method.POST, AppActivity.URL_TRACKING, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try_reload_list_for_service = true;
                        //showProgress(false);
                        Log.e(TAG, String.format("data => %s", response));
                        try {
                            JSONObject json = new JSONObject(response);
                            JSONObject data = json.getJSONObject("data");
                            Message message = handler2.obtainMessage(1,data);
                            message.sendToTarget();
                        } catch (Exception e) {
                            Log.e(TAG,LoginActivity.getStackTrace(e));
                            Message message = handler2.obtainMessage(1,null);
                            message.sendToTarget();
                        }
                        LoginActivity.putSharedStringValue(ctx,AppActivity.KEY_SHARED_GPS_TRACKING,response);

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try_reload_list_for_service = true;
                        String msg = error.getMessage();
                        if(msg==null && error.networkResponse!=null)msg = String.format(Locale.US,"error %d", error.networkResponse.statusCode);
                        Log.e(TAG,msg);
                    }
                }){
                    @Override
                    protected Map<String,String> getParams(){
                        HashMap<String, String> params = new HashMap<String, String>();
                        params.put("date",date);
                        return params;
                    }
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String>  params = new HashMap<String, String>();
                        params.put("Content-Type", "application/x-www-form-urlencoded");
                        params.put("Accept", "application/json");
                        params.put("Authorization", String.format("Bearer %s", token));

                        return params;
                    }
                };
                queue.add(objRequest);
            }catch(Exception e){
                Log.e(TAG,LoginActivity.getStackTrace(e));
            }
        }
    }

    private static class SocketTask extends AsyncTask<String,Void,Void>{

        @Override
        protected Void doInBackground(String... strings) {
            try {
                Socket socket = new Socket(AppActivity.HOST_TRACKING_IP,AppActivity.HOST_TRACKING_PORT);
                DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                String str = strings[0];
                //Log.e(TAG, String.format("doInBackground = %s", str));
                dataOutputStream.writeUTF(str);
            } catch (Exception e) {
                Log.e(TAG,LoginActivity.getStackTrace(e));
            }
            return null;
        }
    }
}


