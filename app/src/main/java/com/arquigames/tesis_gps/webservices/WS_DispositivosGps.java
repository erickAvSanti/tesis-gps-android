package com.arquigames.tesis_gps.webservices;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.arquigames.tesis_gps.AppActivity;
import com.arquigames.tesis_gps.LoginActivity;
import com.arquigames.tesis_gps.dispositivos_gps.DispositivoGpsFragmentForm;
import com.arquigames.tesis_gps.dispositivos_gps.DispositivoGpsFragmentDelete;
import com.arquigames.tesis_gps.dispositivos_gps.DispositivosGpsActivity;
import com.arquigames.tesis_gps.paquetes.PaquetesActivity;
import com.arquigames.tesis_gps.paquetes.ui.main.PaqueteFragmentSelectGpsDevice;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

public class WS_DispositivosGps{
    private static boolean try_reload_list_from_paquetes      = true;
    private static boolean try_reload_list      = true;
    private static boolean try_delete           = true;
    private static boolean try_insert_or_update = true;
    public static String TAG = "WS_DispositivosGps";
    public static void insert_or_update(final DispositivoGpsFragmentForm ctx, HashMap<String,String> data){
        if(!try_insert_or_update){
            return;
        }
        HashMap<String,String> json = LoginActivity.getUserInfo(ctx.getActivity());
        final String token;
        final String brand;
        final String model;
        final String imei;
        final String simcard;
        final String operator;
        final String phone_number;
        final String description;
        final String id;

        if (json.containsKey("token")) {
            token = json.get("token");
        } else {
            token = "";
        }
        if(data.containsKey("brand")){
            brand = data.get("brand");
        }else{
            brand="";
        }
        if(data.containsKey("model")){
            model = data.get("model");
        }else{
            model="";
        }
        if(data.containsKey("imei")){
            imei = data.get("imei");
        }else{
            imei="";
        }
        if(data.containsKey("simcard")){
            simcard = data.get("simcard");
        }else{
            simcard="";
        }
        if(data.containsKey("operator")){
            operator = data.get("operator");
        }else{
            operator="";
        }
        if(data.containsKey("phone_number")){
            phone_number = data.get("phone_number");
        }else{
            phone_number="";
        }
        if(data.containsKey("description")){
            description = data.get("description");
        }else{
            description="";
        }
        if(data.containsKey("id")){
            id = data.get("id");
        }else{
            id="";
        }
        RequestQueue queue = Volley.newRequestQueue(ctx.getActivity());
        try_insert_or_update = false;
        StringRequest objRequest = new StringRequest(id==null || id.isEmpty() ? Request.Method.POST:Request.Method.PUT, id==null || id.isEmpty() ? AppActivity.URL_DISPOSITIVOS_GPS : String.format("%s/%s", AppActivity.URL_DISPOSITIVOS_GPS, id), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try_insert_or_update = true;
                Log.e(WS_DispositivosGps.TAG, response);
                if(id==null || id.isEmpty()){
                    ctx.queryStatus("Registro creado");
                }else{
                    ctx.queryStatus("Registro actualizado");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try_insert_or_update = true;
                if(error.networkResponse.statusCode==401){
                    ctx.queryStatus("Sesión expirada");
                }else{
                    if(id==null || id.isEmpty()){
                        ctx.queryStatus("Error al crear");
                    }else{
                        ctx.queryStatus("Error al actualizar");
                    }
                }
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                HashMap<String, String> params = new HashMap<>();
                if(id!=null)params.put("id", id);
                params.put("brand", brand);
                params.put("model", model);
                params.put("imei", imei);
                params.put("simcard", simcard);
                params.put("phone_number", phone_number);
                params.put("operator", operator);
                params.put("description", description);

                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                params.put("Accept", "application/json");
                params.put("Authorization", String.format("Bearer %s", token));

                return params;
            }
        };
        queue.add(objRequest);
    }
    public static void delete(final DispositivoGpsFragmentDelete ctx, String id){
        if(!try_delete){
            return;
        }
        HashMap<String,String> json = LoginActivity.getUserInfo(ctx.getActivity());
        final String token;

        if (json.containsKey("token")) {
            token = json.get("token");
        } else {
            token = "";
        }
        RequestQueue queue = Volley.newRequestQueue(ctx.getActivity());
        try_delete = false;
        StringRequest objRequest = new StringRequest(Request.Method.DELETE,String.format("%s/%s", AppActivity.URL_DISPOSITIVOS_GPS, id), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try_delete = true;
                Log.e(WS_DispositivosGps.TAG, response);
                ctx.queryStatus("Registro eliminado");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try_delete = true;
                if(error.networkResponse.statusCode==401){
                    ctx.queryStatus("Sesión expirada");
                }else{
                    ctx.queryStatus("Error al eliminar");
                }

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                params.put("Accept", "application/json");
                params.put("Authorization", String.format("Bearer %s", token));

                return params;
            }
        };
        queue.add(objRequest);
    }
    public static void reload(final DispositivosGpsActivity ctx, final int page, final String search, final String order_by_field, final String order_by_cond){
        if(!try_reload_list){
            return;
        }
        HashMap<String,String> json = LoginActivity.getUserInfo(ctx);
        if(json!=null){
            try{
                final String token = json.containsKey("token") ? json.get("token") : "";

                Log.e(DispositivosGpsActivity.TAG, "Reload WS_DispositivosGps");
                try_reload_list = false;
                //showProgress(false);
                RequestQueue queue = Volley.newRequestQueue(ctx);

                StringRequest objRequest = new StringRequest(Request.Method.GET, AppActivity.URL_DISPOSITIVOS_GPS, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try_reload_list = true;
                        //showProgress(false);
                        JSONArray data = null;
                        try {
                            data = new JSONArray(response);
                            LoginActivity.putSharedStringValue(ctx,DispositivosGpsActivity.KEY_SHARED_PREFERENCE,response);
                            ctx.fillList(data);
                        } catch (JSONException e) {
                            Log.e(DispositivosGpsActivity.TAG, LoginActivity.getStackTrace(e));
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try_reload_list = true;
                        //showProgress(false);
                        ctx.stopSwipeRefreshing();
                    }
                }){
                    @Override
                    protected Map<String,String> getParams(){
                        HashMap<String, String> params = new HashMap<String, String>();
                        params.put("page", String.valueOf(page));
                        params.put("search", search);
                        params.put("order_by_field", order_by_field);
                        params.put("order_by_cond",order_by_cond);

                        return params;
                    }
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String>  params = new HashMap<String, String>();
                        params.put("Content-Type", "application/x-www-form-urlencoded");
                        params.put("Accept", "application/json");
                        params.put("Authorization", String.format("Bearer %s", token));

                        return params;
                    }
                };
                queue.add(objRequest);
            }catch(Exception e){

            }
        }
    }
    public static void reload_from_paquetes(final PaqueteFragmentSelectGpsDevice ctx){
        if(!try_reload_list_from_paquetes){
            return;
        }
        HashMap<String,String> json = LoginActivity.getUserInfo(ctx.getContext());
        if(json!=null){
            try{
                final String token = json.containsKey("token") ? json.get("token") : "";

                Log.e(PaquetesActivity.TAG, "Reload WS_Clientes");
                try_reload_list_from_paquetes = false;
                //showProgress(false);
                RequestQueue queue = Volley.newRequestQueue(ctx.getContext());
                StringRequest objRequest = new StringRequest(Request.Method.GET, AppActivity.URL_DISPOSITIVOS_GPS, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try_reload_list_from_paquetes = true;
                        //showProgress(false);
                        JSONArray data = null;
                        try {
                            data = new JSONArray(response);
                            LoginActivity.putSharedStringValue(ctx.getContext(),DispositivosGpsActivity.KEY_SHARED_PREFERENCE,response);
                            ctx.fillList(data);
                            ctx.hideWaitMessage();
                        } catch (JSONException e) {
                            Log.e(PaquetesActivity.TAG, LoginActivity.getStackTrace(e));
                            ctx.queryStatus("Problemas al listar");
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try_reload_list_from_paquetes = true;
                        if(error.networkResponse.statusCode==401){
                            ctx.queryStatus("Sesión expirada");
                        }else{
                            ctx.queryStatus("Error al listar");
                        }
                    }
                }){
                    @Override
                    protected Map<String,String> getParams(){
                        HashMap<String, String> params = new HashMap<String, String>();

                        return params;
                    }
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String>  params = new HashMap<String, String>();
                        params.put("Content-Type", "application/x-www-form-urlencoded");
                        params.put("Accept", "application/json");
                        params.put("Authorization", String.format("Bearer %s", token));

                        return params;
                    }
                };
                queue.add(objRequest);
            }catch(Exception e){

            }
        }
    }
}
