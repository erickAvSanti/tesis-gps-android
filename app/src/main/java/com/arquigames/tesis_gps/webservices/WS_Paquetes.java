package com.arquigames.tesis_gps.webservices;

import android.os.Bundle;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.arquigames.tesis_gps.AppActivity;
import com.arquigames.tesis_gps.LoginActivity;
import com.arquigames.tesis_gps.paquetes.PaqueteFragmentDelete;
import com.arquigames.tesis_gps.paquetes.PaquetesActivity;
import com.arquigames.tesis_gps.paquetes.ui.main.PaqueteFragmentPrincipal;
import com.arquigames.tesis_gps.paquetes.ui.main.PaqueteFragmentProductos;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class WS_Paquetes{
    private static boolean try_reload_list              = true;
    private static boolean try_reload_products_list      = true;
    private static boolean try_delete                   = true;
    private static boolean try_insert_or_update         = true;
    public static String TAG = "WS_Paquetes";
    /*
    public static void insert(final PaqueteFragmentForm ctx, HashMap<String,String> data){
        if(!try_insert_or_update){
            return;
        }
        HashMap<String,String> json = LoginActivity.getUserInfo(ctx.getActivity());
        final String token;
        final String name;
        final String sku;
        final String description;
        final String id;

        if (json.containsKey("token")) {
            token = json.get("token");
        } else {
            token = "";
        }
        if(data.containsKey("name")){
            name = data.get("name");
        }else{
            name="";
        }
        if(data.containsKey("sku")){
            sku = data.get("sku");
        }else{
            sku="";
        }
        if(data.containsKey("description")){
            description = data.get("description");
        }else{
            description="";
        }
        if(data.containsKey("id")){
            id = data.get("id");
        }else{
            id="";
        }
        RequestQueue queue = Volley.newRequestQueue(ctx.getActivity());
        try_insert_or_update = false;
        StringRequest objRequest = new StringRequest(id==null || id.isEmpty() ? Request.Method.POST:Request.Method.PUT, id==null || id.isEmpty() ? AppActivity.URL_PAQUETES : String.format("%s/%s", AppActivity.URL_PAQUETES, id), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try_insert_or_update = true;
                Log.e(WS_Paquetes.TAG, response);
                if(id==null || id.isEmpty()){
                    ctx.queryStatus("Registro creado");
                }else{
                    ctx.queryStatus("Registro actualizado");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try_insert_or_update = true;
                if(error.networkResponse.statusCode==401){
                    ctx.queryStatus("Sesión expirada");
                }else{
                    if(id==null || id.isEmpty()){
                        ctx.queryStatus("Error al crear");
                    }else{
                        ctx.queryStatus("Error al actualizar");
                    }
                }
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                HashMap<String, String> params = new HashMap<>();
                if(id!=null)params.put("id", id);
                params.put("name", name);
                params.put("sku", sku);
                params.put("description", description);

                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                params.put("Accept", "application/json");
                params.put("Authorization", String.format("Bearer %s", token));

                return params;
            }
        };
        queue.add(objRequest);
    }
    */
    public static void delete(final PaqueteFragmentDelete ctx, String id){
        if(!try_delete){
            return;
        }
        HashMap<String,String> json = LoginActivity.getUserInfo(ctx.getActivity());
        final String token;

        if (json.containsKey("token")) {
            token = json.get("token");
        } else {
            token = "";
        }
        RequestQueue queue = Volley.newRequestQueue(ctx.getActivity());
        try_delete = false;
        StringRequest objRequest = new StringRequest(Request.Method.DELETE,String.format("%s/%s", AppActivity.URL_PAQUETES, id), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try_delete = true;
                Log.e(WS_Paquetes.TAG, response);
                ctx.queryStatus("Registro eliminado");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try_delete = true;
                if(error.networkResponse.statusCode==401){
                    ctx.queryStatus("Sesión expirada");
                }else{
                    ctx.queryStatus("Error al eliminar");
                }

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                params.put("Accept", "application/json");
                params.put("Authorization", String.format("Bearer %s", token));

                return params;
            }
        };
        queue.add(objRequest);
    }
    public static void reload(final PaquetesActivity ctx, final int page, final String search, final String order_by_field, final String order_by_cond){
        if(!try_reload_list){
            return;
        }
        HashMap<String,String> json = LoginActivity.getUserInfo(ctx);
        if(json!=null){
            try{
                final String token = json.containsKey("token") ? json.get("token") : "";

                Log.e(PaquetesActivity.TAG, "Reload WS_Paquetes");
                try_reload_list = false;
                //showProgress(false);
                RequestQueue queue = Volley.newRequestQueue(ctx);

                StringRequest objRequest = new StringRequest(Request.Method.GET, AppActivity.URL_PAQUETES, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try_reload_list = true;
                        //showProgress(false);
                        Log.e(TAG, String.format("data => %s", response));
                        JSONArray data = null;
                        try {
                            data = new JSONArray(response);
                            LoginActivity.putSharedStringValue(ctx,PaquetesActivity.KEY_SHARED_PREFERENCE,response);
                            ctx.fillList(data);
                        } catch (JSONException e) {
                            Log.e(PaquetesActivity.TAG, LoginActivity.getStackTrace(e));
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try_reload_list = true;
                        //showProgress(false);
                        ctx.stopSwipeRefreshing();
                    }
                }){
                    @Override
                    protected Map<String,String> getParams(){
                        HashMap<String, String> params = new HashMap<String, String>();
                        params.put("page", String.valueOf(page));
                        params.put("search", search);
                        params.put("order_by_field", order_by_field);
                        params.put("order_by_cond",order_by_cond);

                        return params;
                    }
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String>  params = new HashMap<String, String>();
                        params.put("Content-Type", "application/x-www-form-urlencoded");
                        params.put("Accept", "application/json");
                        params.put("Authorization", String.format("Bearer %s", token));

                        return params;
                    }
                };
                queue.add(objRequest);
            }catch(Exception e){

            }
        }
    }

    public static void insert_or_update(final PaqueteFragmentPrincipal paqueteFragmentPrincipal, final Bundle data) {
        if(!try_insert_or_update){
            return;
        }
        HashMap<String,String> json = LoginActivity.getUserInfo(paqueteFragmentPrincipal.getActivity());
        final String token;
        final String name;
        final String sku;
        final String description;
        final String id;

        if (json.containsKey("token")) {
            token = json.get("token");
        } else {
            token = "";
        }
        if(data.containsKey("id")){
            id = data.getString("id");
        }else{
            id="";
        }
        RequestQueue queue = Volley.newRequestQueue(paqueteFragmentPrincipal.getActivity());
        try_insert_or_update = false;
        StringRequest objRequest = new StringRequest(id==null || id.isEmpty() ? Request.Method.POST:Request.Method.PUT, id==null || id.isEmpty() ? AppActivity.URL_PAQUETES : String.format("%s/%s", AppActivity.URL_PAQUETES, id), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try_insert_or_update = true;
                Log.e(TAG, response);
                if(id==null || id.isEmpty()){
                    paqueteFragmentPrincipal.queryStatus("Registro creado");
                }else{
                    paqueteFragmentPrincipal.queryStatus("Registro actualizado");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try_insert_or_update = true;
                if(error.networkResponse.statusCode==401){
                    paqueteFragmentPrincipal.queryStatus("Sesión expirada");
                }else{
                    if(id==null || id.isEmpty()){
                        paqueteFragmentPrincipal.queryStatus("Error al crear");
                    }else{
                        paqueteFragmentPrincipal.queryStatus("Error al actualizar");
                    }
                }
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                HashMap<String, String> params = new HashMap<>();
                Iterator<String> it = data.keySet().iterator();
                String key;
                while (it.hasNext()) {
                    key = it.next();
                    String val = data.getString(key);
                    if(val!=null)params.put(key, val);
                }
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                params.put("Accept", "application/json");
                params.put("Authorization", String.format("Bearer %s", token));

                return params;
            }
        };
        queue.add(objRequest);
    }

    public static void reload_productos(final PaqueteFragmentProductos ctx, String id) {

        if (!try_reload_products_list) {
            return;
        }
        HashMap<String,String> json = LoginActivity.getUserInfo(ctx.getActivity());
        if (json != null) {
            try {
                final String token = json.containsKey("token") ? json.get("token") : "";

                Log.e(PaquetesActivity.TAG, "Reload WS_Paquetes productos");
                try_reload_products_list = false;
                //showProgress(false);
                RequestQueue queue = Volley.newRequestQueue(ctx.getActivity());

                StringRequest objRequest = new StringRequest(Request.Method.GET, String.format("%s%s", AppActivity.URL_PAQUETES_PRODUCTOS, id), new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try_reload_products_list = true;
                        //showProgress(false);
                        JSONObject data;
                        boolean pass = false;
                        JSONArray arr = new JSONArray();
                        try {
                            data = new JSONObject(response);
                            if(data.has("get_products")){
                                Object tmp = data.get("get_products");
                                if(tmp instanceof JSONArray){
                                    arr = (JSONArray)tmp;
                                    pass = true;
                                }
                            }
                            if(!pass)arr = new JSONArray();
                            ctx.fillList(arr);
                        } catch (JSONException e) {
                            Log.e(PaquetesActivity.TAG, LoginActivity.getStackTrace(e));
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try_reload_products_list = true;
                        //showProgress(false);
                        ctx.stopSwipeRefreshing();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        HashMap<String, String> params = new HashMap<String, String>();

                        return params;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Content-Type", "application/x-www-form-urlencoded");
                        params.put("Accept", "application/json");
                        params.put("Authorization", String.format("Bearer %s", token));

                        return params;
                    }
                };
                queue.add(objRequest);
            } catch (Exception e) {

            }
        }
    }
}


