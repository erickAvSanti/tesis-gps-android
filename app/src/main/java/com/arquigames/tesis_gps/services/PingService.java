package com.arquigames.tesis_gps.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.arquigames.tesis_gps.LoginActivity;
import com.arquigames.tesis_gps.MapSettingsFragment;
import com.arquigames.tesis_gps.R;
import com.arquigames.tesis_gps.webservices.WS_Tracking;

import org.json.JSONObject;

import java.util.ArrayList;

import static com.arquigames.tesis_gps.AppActivity.CHANNEL_ID;

public class PingService extends Service {
    private IBinder mBinder = new LocalBinder();
    private Looper serviceLooper;
    private ServiceHandler serviceHandler;
    private JSONObject content;
    public static final String TAG = "PingService";

    public static int notificationId = 1;
    private ArrayList<Integer> notificationIds = new ArrayList<>();

    public PingService() {
    }
    public Looper getServiceLooper(){
        return serviceLooper;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Message message = serviceHandler.obtainMessage();
        message.arg1 = startId;
        serviceHandler.sendMessage(message);
        return START_STICKY;
    }

        @Override
    public void onCreate() {
        HandlerThread thread = new HandlerThread("PingServiceThread", Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();
        serviceLooper = thread.getLooper();
        serviceHandler = new ServiceHandler(serviceLooper);
    }

    @Override
    public void onDestroy() {
        serviceHandler.stopCustomRunnable();
        serviceHandler.removeCallbacksAndMessages(null);
        serviceLooper.quit();
        serviceLooper = null;
        serviceHandler = null;
    }

    public void setContent(JSONObject jsonObject) {
        this.content = jsonObject;

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_live_help_black_24dp)
                .setContentTitle("My notification")
                .setContentText("Much longer text that cannot fit one line...")
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText("Much longer text that cannot fit one line..."))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);


        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationIds.add(notificationId);
        // notificationId is a unique int for each notification that you must define
        notificationManager.notify(notificationId++, builder.build());
    }

    public JSONObject getContent() {
        return content;
    }

    public class LocalBinder extends Binder{
        public PingService getService(){
            return PingService.this;
        }
    }
    private void queryDataServer() {
        String date = LoginActivity.getSharedStringValue(this, MapSettingsFragment.GPS_TRACKING_DATE);
        WS_Tracking.listForService(this,date);
    }
    private final class ServiceHandler extends Handler{
        private Runnable runnable = null;

        public ServiceHandler(Looper looper){
            super(looper);
        }
        @Override
        public void handleMessage(Message msg) {
            runnable = new Runnable() {
                @Override
                public void run() {
                    queryDataServer();
                    ServiceHandler.this.postDelayed(this, 6000);
                }
            };
            this.postDelayed(runnable, 3000);
        }
        public void stopCustomRunnable(){
            removeCallbacks(runnable);
        }
    }
}
