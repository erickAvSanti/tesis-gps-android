package com.arquigames.tesis_gps.productos;

import android.content.Context;
import android.util.AttributeSet;

public class ProductoNameTextView extends android.support.v7.widget.AppCompatTextView {
    private String producto_id;
    public ProductoNameTextView(Context context) {
        super(context);
    }

    public ProductoNameTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ProductoNameTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public String getProducto_id() {
        return producto_id;
    }

    public void setProducto_id(String producto_id) {
        this.producto_id = producto_id;
    }
}
