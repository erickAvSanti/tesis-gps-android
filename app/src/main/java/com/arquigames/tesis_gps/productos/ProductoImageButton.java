package com.arquigames.tesis_gps.productos;

import android.content.Context;
import android.support.v7.widget.AppCompatImageButton;
import android.util.AttributeSet;

public class ProductoImageButton extends AppCompatImageButton {
    private String producto_delete_id;
    public ProductoImageButton(Context context) {
        super(context);
    }

    public ProductoImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ProductoImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public String getProducto_delete_id() {
        return producto_delete_id;
    }

    public void setProducto_delete_id(String producto_delete_id) {
        this.producto_delete_id = producto_delete_id;
    }
}
