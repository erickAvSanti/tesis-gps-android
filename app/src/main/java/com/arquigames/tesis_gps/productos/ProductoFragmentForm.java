package com.arquigames.tesis_gps.productos;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.arquigames.tesis_gps.R;
import com.arquigames.tesis_gps.webservices.WS_Productos;
import com.arquigames.tesis_gps.webservices.WS_Productos;

import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProductoFragmentForm.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProductoFragmentForm#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProductoFragmentForm extends AppCompatDialogFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "ProductoFragmentForm";
    private static String ARG_NAME = "name";
    private static String ARG_EMAIL = "email";

    // TODO: Rename and change types of parameters
    private EditText text_name;
    private EditText text_sku;
    private EditText text_description;
    private TextView textview_wait_message;

    private Button btn_go;
    private Button btn_cancel;

    private OnFragmentInteractionListener mListener;
    private Bundle bundle;
    private TextView text_title;

    public ProductoFragmentForm() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProductoFragmentForm.
     */
    // TODO: Rename and change types and number of parameters
    public static ProductoFragmentForm newInstance(String param1, String param2) {
        ProductoFragmentForm fragment = new ProductoFragmentForm();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View root = inflater.inflate(R.layout.dialog_fragment_productos_form, null);
        builder.setView(root);





        bundle = getArguments();
        text_title          = root.findViewById(R.id.productos_dialog_title);
        text_name           = root.findViewById(R.id.productos_dialog_name);
        text_sku            = root.findViewById(R.id.productos_dialog_sku);
        text_description    = root.findViewById(R.id.productos_dialog_description);
        textview_wait_message = root.findViewById(R.id.productos_dialog_wait_message);
        this.hideWaitMessage();

        text_sku.setText(bundle.getString("sku"));
        text_description.setText(bundle.getString("description"));
        text_name.setText(bundle.getString("name"));

        btn_go      = root.findViewById(R.id.productos_dialog_go);
        btn_cancel  = root.findViewById(R.id.productos_dialog_cancel);
        final int action;
        action = bundle.getInt("new",1);
        if(action==1){
            text_title.setText("Nuevo producto");
            btn_go.setText(R.string.register);
        }else{
            text_title.setText("Detalles de producto");
            btn_go.setText(R.string.update);
        }
        btn_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProductoFragmentForm.this.waitMessage(1);

                HashMap<String,String> data = new HashMap<String,String>();
                data.put("name",text_name.getText().toString());
                data.put("sku",text_sku.getText().toString());
                data.put("description",text_description.getText().toString());
                data.put("id",bundle.getString("id"));

                WS_Productos.insert_or_update(ProductoFragmentForm.this,data);
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProductoFragmentForm.this.getDialog().cancel();
            }
        });


        if(savedInstanceState!=null){
            String title = savedInstanceState.getString("title","");
            String sku = savedInstanceState.getString("sku","");
            String description = savedInstanceState.getString("description","");
            if(!title.equals(""))text_title.setText(title);
            if(!sku.equals(""))text_sku.setText(sku);
            if(!description.equals(""))text_description.setText(description);
        }


        return builder.create();
    }

    private void waitMessage(int state) {
        textview_wait_message.setText(R.string.wait_please);
        textview_wait_message.setVisibility(View.VISIBLE);
    }
    private void hideWaitMessage() {
        textview_wait_message.setVisibility(View.GONE);
    }
    public void queryStatus(String status){
        textview_wait_message.setText(status);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(text_name!=null && text_name.getText()!=null){
            outState.putString("name", text_name.getText().toString());
        }
        if(text_sku!=null && text_sku.getText()!=null){
            outState.putString("sku", text_sku.getText().toString());
        }
        if(text_description!=null && text_description.getText()!=null){
            outState.putString("description", text_description.getText().toString());
        }
    }
    @Override
    public void onResume() {

        super.onResume();
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
