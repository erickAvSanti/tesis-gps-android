package com.arquigames.tesis_gps.broadcastreceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.arquigames.tesis_gps.services.RecordService;

public class RestarterRecordService extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("Broadcast Listened", "Service tried to stop");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(new Intent(context, RecordService.class));
        } else {
            context.startService(new Intent(context, RecordService.class));
        }
    }
}