package com.arquigames.tesis_gps;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MapSettingsFragment extends AppCompatDialogFragment {
    public static final String GPS_TRACKING_DATE = "gps_tracking_date";
    public static final String GPS_TRACKING_IMEI = "gps_tracking_imei";
    public static final String GPS_TRACKING_USERS_ID= "gps_tracking_users_id";
    private static final String TAG = "MapSettingsFragment";
    private Bundle bundle;
    private MapFragmentInteraction mapInteraction;
    private DatePicker date;
    private String map_selected_date = "";
    private String map_selected_imei = "";
    private String map_selected_users_id = "";
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        android.support.v7.app.AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.dialog_fragment_maps_settings, null));
        return builder.create();
    }
    @Override
    public void onAttach(Context ctx) {
        if(ctx instanceof MapFragmentInteraction){
            mapInteraction = (MapFragmentInteraction)ctx;

        }
        super.onAttach(ctx);
    }
    @Override
    public void onResume() {

        super.onResume();
        bundle = getArguments();
        AppCompatButton btn = this.getDialog().findViewById(R.id.accept);
        date = this.getDialog().findViewById(R.id.date);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MapSettingsFragment.this.processOptions();
            }
        });
        map_selected_date       = LoginActivity.getSharedStringValue(this.getActivity(),MapSettingsFragment.GPS_TRACKING_DATE);
        map_selected_imei       = LoginActivity.getSharedStringValue(this.getActivity(),MapSettingsFragment.GPS_TRACKING_IMEI);
        map_selected_users_id   = LoginActivity.getSharedStringValue(this.getActivity(),MapSettingsFragment.GPS_TRACKING_USERS_ID);

        Log.e(TAG, String.format("restoring ... map_selected_date = %s", map_selected_date));
        String pattern = "^(\\d{4})-(\\d{1,2})-(\\d{1,2})$";
        if(map_selected_date.matches(pattern)){
            Pattern r = Pattern.compile(pattern);
            Matcher m = r.matcher(map_selected_date);
            if(m.find()){
                int year = Integer.parseInt(m.group(1));
                int month = Integer.parseInt(m.group(2));
                int day = Integer.parseInt(m.group(3));
                date.updateDate(year,month-1,day);
            }
            //date.updateDate();
        }
    }

    private void processOptions() {
        map_selected_date = String.format(Locale.US,"%d-%d-%d", date.getYear(), date.getMonth() + 1, date.getDayOfMonth());
        Log.e(TAG, String.format("saving.. map_selected_date%s", map_selected_date));
        LoginActivity.putSharedStringValue(MapSettingsFragment.this.getActivity(),GPS_TRACKING_DATE,map_selected_date);



        mapInteraction.onMapDialogClosing(null);
    }

    public interface MapFragmentInteraction{
        void onMapDialogClosing(Bundle bundle);
    }
}
