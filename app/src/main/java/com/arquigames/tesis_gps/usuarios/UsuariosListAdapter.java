package com.arquigames.tesis_gps.usuarios;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.arquigames.tesis_gps.LoginActivity;
import com.arquigames.tesis_gps.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

public class UsuariosListAdapter extends ArrayAdapter<JSONObject> {
    private static final String TAG = "UsuariosListAdapter";
    private UsuariosActivity ctx;
    private int resource;
    private HashMap<String, JSONObject> mapObjects = new HashMap<String,JSONObject>();

    public UsuariosListAdapter(UsuariosActivity context, int textViewResourceId,
                              List<JSONObject> objects) {
        super(context, textViewResourceId, objects);
        ctx = context;
        resource = textViewResourceId;
        for (int i = 0; i < objects.size(); ++i) {
            JSONObject obj = objects.get(i);
            try {
                String id = obj.getString("id");
                Log.e("Init ID",id);
                mapObjects.put(id,obj);
            } catch (JSONException e) {
                Log.e(TAG, LoginActivity.getStackTrace(e));
            }
        }
    }

    @Override
    public long getItemId(int position) {
        JSONObject item = getItem(position);
        int id = 0;
        try {
            id = Integer.parseInt(item.getString("id"));
            Log.e("getITemId",String.valueOf(id));
        } catch (JSONException e) {
            Log.e(TAG,LoginActivity.getStackTrace(e));
        }
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(ctx);
            v = vi.inflate(resource, null);
        }

        JSONObject user = getItem(position);

        if (user != null) {
            UsuarioNameTextView name = v.findViewById(R.id.usuarios_list_name);
            UsuarioImageButton close = v.findViewById(R.id.usuarios_list_delete);
            try {
                name.setText(user.getString("name"));
                name.setUsuario_id(user.getString("id"));
                close.setUsuario_delete_id(user.getString("id"));
                name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ctx.editUsuario(((UsuarioNameTextView)v).getUsuario_id());
                    }
                });
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ctx.deleteUsuario(((UsuarioImageButton)v).getUsuario_delete_id());
                    }
                });
            } catch (JSONException e) {
                Log.e(TAG,LoginActivity.getStackTrace(e));
                name.setText("Error");
            }
        }

        return v;
    }


    @Override
    public boolean hasStableIds() {
        return true;
    }
}
