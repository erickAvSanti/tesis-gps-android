package com.arquigames.tesis_gps.usuarios;

import android.content.Context;
import android.util.AttributeSet;

public class UsuarioNameTextView extends android.support.v7.widget.AppCompatTextView {
    private String usuario_id;
    public UsuarioNameTextView(Context context) {
        super(context);
    }

    public UsuarioNameTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public UsuarioNameTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public String getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(String usuario_id) {
        this.usuario_id = usuario_id;
    }
}
