package com.arquigames.tesis_gps.usuarios;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.arquigames.tesis_gps.AppActivity;
import com.arquigames.tesis_gps.LoginActivity;
import com.arquigames.tesis_gps.R;
import com.arquigames.tesis_gps.webservices.WS_Usuarios;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UsuariosActivity extends AppCompatActivity {

    private Handler handler = new Handler();
    private ArrayList<String> list;
    private HashMap<String,JSONObject> list_map;
    private UsuariosListAdapter usuarioAdapter;
    private ListView swipe_list;
    private SwipeRefreshLayout swipe;
    public static String TAG = "CallRecordActivity";
    public static String KEY_SHARED_PREFERENCE  ="usuarios_result";

    private int pagination = 0;
    private int page = 1;
    private String search = "";
    private String order_by_field = "";
    private String order_by_cond = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuarios);


        Intent intent_extras = this.getIntent();
        String source = "";
        if(intent_extras!=null){
            source = intent_extras.getStringExtra("result");
            if(source!="" && source!=null){
                LoginActivity.putSharedStringValue(this,KEY_SHARED_PREFERENCE,source);
            }else{
                source = LoginActivity.getSharedStringValue(this,KEY_SHARED_PREFERENCE);
            }
        }else{
            source = LoginActivity.getSharedStringValue(this,KEY_SHARED_PREFERENCE);
        }
        if(source!=null)
            source = source.trim();
        else
            source="";

        JSONArray arr = new JSONArray();
        try {
            if(!source.isEmpty()){
                Log.e(TAG,"SOURCE => "+source);
                arr = new JSONArray(source);
                page = 1;
                pagination = 20;
            }
        } catch (JSONException e) {
            Log.e(TAG,LoginActivity.getStackTrace(e));
        }


        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        swipe = findViewById(R.id.swipe_usuarios);
        swipe_list = findViewById(R.id.swipe_usuarios_list);

        swipe.setColorSchemeResources(R.color.color_swipe_list_foreground);
        swipe.setProgressBackgroundColorSchemeResource(R.color.color_swipe_list_background);

        if(source.isEmpty()){
            this.getList();
        }
        SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
            UsuariosActivity.this.getList();
            }
        };
        swipe.setOnRefreshListener(onRefreshListener);
        this.fillList(arr);
    }

    public void getList() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                UsuariosActivity.this.getData();
            }
        });
    }

    public void fillList(JSONArray arr){
        List<JSONObject> values = new ArrayList<>();
        if(list_map!=null){
            list_map.clear();
        }else{
            list_map = new HashMap<>();
        }
        if(arr.length()>0){
            int pag = (page-1)*pagination;
            for(int i=0;i<arr.length();i++){
                try {
                    JSONObject obj = arr.getJSONObject(i);
                    obj = AppActivity.cleanJSONObject(obj);
                    values.add(obj);
                    list_map.put(obj.getString("id"),obj);
                } catch (JSONException e) {
                    Log.e(TAG,LoginActivity.getStackTrace(e));
                }
            }
        }
        usuarioAdapter = new UsuariosListAdapter(this, R.layout.usuarios_list_item, values);
        swipe_list.setAdapter(usuarioAdapter);
        notifySwipeAdapter();
    }
    private void getData(){
        WS_Usuarios.reload(UsuariosActivity.this,page,search,order_by_field,order_by_cond);
    }
    public void stopSwipeRefreshing(){
        swipe.setRefreshing(false);
    }
    public void notifySwipeAdapter(){
        stopSwipeRefreshing();
        usuarioAdapter.notifyDataSetChanged();
    }

    public void setPage(int page) {
        this.page = page;
    }
    public void previousPage(View view){
        this.page--;
        if(page<1)page=1;
        if(!swipe.isRefreshing())swipe.setRefreshing(true);
        getData();
    }
    public void nextPage(View view){
        this.page++;
        if(!swipe.isRefreshing())swipe.setRefreshing(true);
        getData();
    }
    public void createUsuario(View view){
        // Create an instance of the dialog fragment and show it
        UsuarioFragmentForm dialog = new UsuarioFragmentForm();
        Bundle bundle = new Bundle();
        bundle.putInt("new",1);
        dialog.setArguments(bundle);
        dialog.show(getSupportFragmentManager(), "UsuarioFragmentForm");

    }
    public void editUsuario(JSONObject usuarioInfo){
        // Create an instance of the dialog fragment and show it
        Bundle bundle = this.getBundleUsuario(usuarioInfo);
        if(bundle.size()>0){
            bundle.putInt("new",0);
            UsuarioFragmentForm dialog = new UsuarioFragmentForm();
            dialog.setArguments(bundle);
            dialog.show(getSupportFragmentManager(), "UsuarioFragmentForm");
        }

    }
    public void setPagination(int pag) {
        pagination = pag;
    }

    public void editUsuario(String usuario_id) {
        this.editUsuario(list_map.get(usuario_id));
    }
    private Bundle getBundleUsuario(JSONObject usuarioInfo){

        Bundle bundle = new Bundle();
        try {
            bundle.putString("role",usuarioInfo.getString("role"));
            bundle.putString("name",usuarioInfo.getString("name"));
            bundle.putString("email",usuarioInfo.getString("email"));
            bundle.putString("id",usuarioInfo.getString("id"));
        } catch (JSONException e) {
            Log.e(TAG,LoginActivity.getStackTrace(e));
        }
        return bundle;
    }

    public void deleteUsuario(String usuario_id) {
        JSONObject usuarioInfo = list_map.get(usuario_id);
        Bundle bundle = this.getBundleUsuario(usuarioInfo);
        if(bundle.size()>0){
            UsuarioFragmentDelete dialog = new UsuarioFragmentDelete();
            dialog.setArguments(bundle);
            dialog.show(getSupportFragmentManager(), "UsuarioFragmentDelete");
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            NavUtils.navigateUpTo(this, new Intent(this, AppActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
