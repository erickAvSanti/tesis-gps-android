package com.arquigames.tesis_gps.usuarios;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.arquigames.tesis_gps.R;
import com.arquigames.tesis_gps.webservices.WS_Usuarios;

import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link UsuarioFragmentForm.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link UsuarioFragmentForm#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UsuarioFragmentForm extends AppCompatDialogFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "UsuarioFragmentForm";
    private static String ARG_NAME = "name";
    private static String ARG_EMAIL = "email";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String name;
    private String email;

    private EditText text_name;
    private EditText text_email;
    private EditText text_password;
    private TextView textview_wait_message;
    private Spinner spinner_role;

    private Button btn_go;
    private Button btn_cancel;

    private OnFragmentInteractionListener mListener;
    private Bundle bundle;
    private TextView text_title;

    public UsuarioFragmentForm() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UsuarioFragmentForm.
     */
    // TODO: Rename and change types and number of parameters
    public static UsuarioFragmentForm newInstance(String param1, String param2) {
        UsuarioFragmentForm fragment = new UsuarioFragmentForm();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View root = inflater.inflate(R.layout.dialog_fragment_usuarios_form, null);
        builder.setView(root);



        bundle = getArguments();
        text_title       = root.findViewById(R.id.usuarios_dialog_title);
        text_name       = root.findViewById(R.id.usuarios_dialog_name);
        text_email      = root.findViewById(R.id.usuarios_dialog_email);
        text_password   = root.findViewById(R.id.usuarios_dialog_password);
        textview_wait_message = root.findViewById(R.id.usuarios_dialog_wait_message);
        spinner_role = root.findViewById(R.id.usuarios_dialog_role);
        this.hideWaitMessage();

        text_email.setText(bundle.getString("email"));
        text_name.setText(bundle.getString("name"));

        btn_go      = root.findViewById(R.id.usuarios_dialog_go);
        btn_cancel  = root.findViewById(R.id.usuarios_dialog_cancel);
        final int action;
        action = bundle.getInt("new",1);
        if(action==1){
            text_title.setText("Nuevo usuario");
            btn_go.setText(R.string.register);
        }else{
            text_title.setText("Detalles de usuario");
            btn_go.setText(R.string.update);
        }
        this.setSelectedRole(spinner_role,bundle.getString("role"));
        btn_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UsuarioFragmentForm.this.waitMessage(1);

                HashMap<String,String> data = new HashMap<String,String>();
                data.put("name",text_name.getText().toString());
                data.put("email",text_email.getText().toString());
                data.put("password",text_password.getText().toString());
                data.put("id",bundle.getString("id"));
                data.put("role",spinner_role.getSelectedItem().toString());

                WS_Usuarios.insert_or_update(UsuarioFragmentForm.this,data);
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UsuarioFragmentForm.this.getDialog().cancel();
            }
        });




        if(savedInstanceState!=null){
            String name = savedInstanceState.getString("name","");
            String email = savedInstanceState.getString("email","");
            String password = savedInstanceState.getString("password","");
            String role = savedInstanceState.getString("role","");
            if(!name.equals(""))text_name.setText(name);
            if(!email.equals(""))text_email.setText(email);
            if(!password.equals(""))text_password.setText(password);
            if(!role.equals(""))this.setSelectedRole(spinner_role,role);
        }


        return builder.create();
    }

    private void waitMessage(int state) {
        textview_wait_message.setText(R.string.wait_please);
        textview_wait_message.setVisibility(View.VISIBLE);
    }
    private void hideWaitMessage() {
        textview_wait_message.setVisibility(View.GONE);
    }
    public void queryStatus(String status){
        textview_wait_message.setText(status);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = getArguments();
        if (bundle != null) {
            ARG_NAME = getArguments().getString(ARG_NAME);
            ARG_EMAIL = getArguments().getString(ARG_EMAIL);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(text_name!=null && text_name.getText()!=null){
            outState.putString("name", text_name.getText().toString());
        }
        if(text_email!=null && text_email.getText()!=null){
            outState.putString("email", text_email.getText().toString());
        }
        if(text_password!=null && text_password.getText()!=null){
            outState.putString("password", text_password.getText().toString());
        }
        if(spinner_role!=null && spinner_role.getSelectedItem().toString()!=null){
            outState.putString("role", spinner_role.getSelectedItem().toString());
        }
    }

    @Override
    public void onResume() {

        super.onResume();

    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
    private void setSelectedRole(Spinner spinner, String value) {
        ArrayAdapter adapter = (ArrayAdapter) spinner.getAdapter();
        for (int position = 0; position < adapter.getCount(); position++) {
            if(adapter.getItem(position).toString().equals(value)) {
                spinner.setSelection(position);
                return;
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
