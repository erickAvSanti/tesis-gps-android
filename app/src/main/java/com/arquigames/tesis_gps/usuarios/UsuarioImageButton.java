package com.arquigames.tesis_gps.usuarios;

import android.content.Context;
import android.support.v7.widget.AppCompatImageButton;
import android.util.AttributeSet;

public class UsuarioImageButton extends AppCompatImageButton {
    private String usuario_delete_id;
    public UsuarioImageButton(Context context) {
        super(context);
    }

    public UsuarioImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public UsuarioImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public String getUsuario_delete_id() {
        return usuario_delete_id;
    }

    public void setUsuario_delete_id(String usuario_delete_id) {
        this.usuario_delete_id = usuario_delete_id;
    }
}
