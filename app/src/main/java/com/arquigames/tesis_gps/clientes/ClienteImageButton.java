package com.arquigames.tesis_gps.clientes;

import android.content.Context;
import android.support.v7.widget.AppCompatImageButton;
import android.util.AttributeSet;

public class ClienteImageButton extends AppCompatImageButton {
    private String cliente_delete_id;
    public ClienteImageButton(Context context) {
        super(context);
    }

    public ClienteImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ClienteImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public String getCliente_delete_id() {
        return cliente_delete_id;
    }

    public void setCliente_delete_id(String cliente_delete_id) {
        this.cliente_delete_id = cliente_delete_id;
    }
}
