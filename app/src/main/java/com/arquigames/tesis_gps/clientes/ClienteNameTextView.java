package com.arquigames.tesis_gps.clientes;

import android.content.Context;
import android.util.AttributeSet;

public class ClienteNameTextView extends android.support.v7.widget.AppCompatTextView {
    private String cliente_id;
    public ClienteNameTextView(Context context) {
        super(context);
    }

    public ClienteNameTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ClienteNameTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public String getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(String cliente_id) {
        this.cliente_id = cliente_id;
    }
}
