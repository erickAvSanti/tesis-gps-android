package com.arquigames.tesis_gps.clientes;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.arquigames.tesis_gps.AppActivity;
import com.arquigames.tesis_gps.LoginActivity;
import com.arquigames.tesis_gps.R;
import com.arquigames.tesis_gps.webservices.WS_Clientes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class ClientesActivity extends AppCompatActivity {

    private Handler handler = new Handler();
    private ArrayList<String> list;
    private HashMap<String,JSONObject> list_map;
    private ClientesListAdapter productoAdapter;
    private ListView swipe_list;
    private SwipeRefreshLayout swipe;
    public static String TAG = "ClientesActivity";
    public static String KEY_SHARED_PREFERENCE  ="clientes_result";

    private int pagination = 0;
    private int page = 1;
    private String search = "";
    private String order_by_field = "";
    private String order_by_cond = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clientes);


        Intent intent_extras = this.getIntent();
        String source = "";
        if(intent_extras!=null){
            source = intent_extras.getStringExtra("result");
            if(source!="" && source!=null){
                LoginActivity.putSharedStringValue(this,KEY_SHARED_PREFERENCE,source);
            }else{
                source = LoginActivity.getSharedStringValue(this,KEY_SHARED_PREFERENCE);
            }
        }else{
            source = LoginActivity.getSharedStringValue(this,KEY_SHARED_PREFERENCE);
        }
        if(source!=null)
            source = source.trim();
        else
            source="";

        JSONArray arr = new JSONArray();
        try {
            if(!source.isEmpty()){
                Log.e(TAG,"SOURCE => "+source);
                arr = new JSONArray(source);
                page = 1;
                pagination = 20;
            }
        } catch (JSONException e) {
            Log.e(TAG,LoginActivity.getStackTrace(e));
        }


        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        swipe = findViewById(R.id.swipe_clientes);
        swipe_list = findViewById(R.id.swipe_clientes_list);

        swipe.setColorSchemeResources(R.color.color_swipe_list_foreground);
        swipe.setProgressBackgroundColorSchemeResource(R.color.color_swipe_list_background);

        if(source.isEmpty()){
            this.getList();
        }
        SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ClientesActivity.this.getList();
            }
        };
        swipe.setOnRefreshListener(onRefreshListener);
        this.fillList(arr);
    }

    public void getList() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                ClientesActivity.this.getData();
            }
        });
    }

    public void fillList(JSONArray arr){
        List<JSONObject> values = new ArrayList<>();
        if(list_map!=null){
            list_map.clear();
        }else{
            list_map = new HashMap<>();
        }
        if(arr.length()>0){
            int pag = (page-1)*pagination;
            for(int i=0;i<arr.length();i++){
                try {
                    JSONObject obj = arr.getJSONObject(i);
                    obj = AppActivity.cleanJSONObject(obj);
                    values.add(obj);
                    list_map.put(obj.getString("id"),obj);
                } catch (JSONException e) {
                    Log.e(TAG,LoginActivity.getStackTrace(e));
                }
            }
        }
        productoAdapter = new ClientesListAdapter(this, R.layout.clientes_list_item, values);
        swipe_list.setAdapter(productoAdapter);
        notifySwipeAdapter();
    }
    private void getData(){
        WS_Clientes.reload(ClientesActivity.this,page,search,order_by_field,order_by_cond);
    }
    public void stopSwipeRefreshing(){
        swipe.setRefreshing(false);
    }
    public void notifySwipeAdapter(){
        stopSwipeRefreshing();
        productoAdapter.notifyDataSetChanged();
    }

    public void setPage(int page) {
        this.page = page;
    }
    public void previousPage(View view){
        this.page--;
        if(page<1)page=1;
        if(!swipe.isRefreshing())swipe.setRefreshing(true);
        getData();
    }
    public void nextPage(View view){
        this.page++;
        if(!swipe.isRefreshing())swipe.setRefreshing(true);
        getData();
    }
    public void createCliente(View view){
        // Create an instance of the dialog fragment and show it
        ClienteFragmentForm dialog = new ClienteFragmentForm();
        Bundle bundle = new Bundle();
        bundle.putInt("new",1);
        dialog.setArguments(bundle);
        dialog.show(getSupportFragmentManager(), "ClienteFragmentForm");

    }
    public void editCliente(JSONObject productoInfo){
        // Create an instance of the dialog fragment and show it
        Bundle bundle = this.getBundleCliente(productoInfo);
        if(bundle.size()>0){
            bundle.putInt("new",0);
            ClienteFragmentForm dialog = new ClienteFragmentForm();
            dialog.setArguments(bundle);
            dialog.show(getSupportFragmentManager(), "ClienteFragmentForm");
        }

    }
    public void setPagination(int pag) {
        pagination = pag;
    }

    public void editCliente(String producto_id) {
        this.editCliente(list_map.get(producto_id));
    }
    private Bundle getBundleCliente(JSONObject productoInfo){

        Bundle bundle = new Bundle();
        try {
            bundle.putString("document",productoInfo.getString("document"));
            bundle.putString("firstname",productoInfo.getString("firstname"));
            bundle.putString("lastname",productoInfo.getString("lastname"));
            bundle.putString("email",productoInfo.getString("email"));
            bundle.putString("phone_number",productoInfo.getString("phone_number"));
            bundle.putString("address",productoInfo.getString("address"));
            bundle.putString("id",productoInfo.getString("id"));
        } catch (JSONException e) {
            Log.e(TAG,LoginActivity.getStackTrace(e));
        }
        return bundle;
    }

    public void deleteCliente(String producto_id) {
        JSONObject productoInfo = list_map.get(producto_id);
        Bundle bundle = this.getBundleCliente(productoInfo);
        if(bundle.size()>0){
            ClienteFragmentDelete dialog = new ClienteFragmentDelete();
            dialog.setArguments(bundle);
            dialog.show(getSupportFragmentManager(), "ClienteFragmentDelete");
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            NavUtils.navigateUpTo(this, new Intent(this, AppActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
