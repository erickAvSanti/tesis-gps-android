package com.arquigames.tesis_gps.clientes;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.arquigames.tesis_gps.R;
import com.arquigames.tesis_gps.webservices.WS_Clientes;
import com.arquigames.tesis_gps.webservices.WS_Clientes;

import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ClienteFragmentForm.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ClienteFragmentForm#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ClienteFragmentForm extends AppCompatDialogFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "ClienteFragmentForm";
    private static String ARG_NAME = "name";
    private static String ARG_EMAIL = "email";

    // TODO: Rename and change types of parameters
    private EditText text_firstname;
    private EditText text_lastname;
    private EditText text_document;
    private EditText text_email;
    private EditText text_phone_number;
    private EditText text_address;
    private TextView textview_wait_message;

    private Button btn_go;
    private Button btn_cancel;

    private OnFragmentInteractionListener mListener;
    private Bundle bundle;
    private TextView text_title;

    public ClienteFragmentForm() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ClienteFragmentForm.
     */
    // TODO: Rename and change types and number of parameters
    public static ClienteFragmentForm newInstance(String param1, String param2) {
        ClienteFragmentForm fragment = new ClienteFragmentForm();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View root = inflater.inflate(R.layout.dialog_fragment_clientes_form, null);
        builder.setView(root);

        bundle = getArguments();
        text_title          = root.findViewById(R.id.clientes_dialog_title);
        text_firstname = root.findViewById(R.id.clientes_dialog_firstname);
        text_lastname = root.findViewById(R.id.clientes_dialog_lastname);
        text_document = root.findViewById(R.id.clientes_dialog_document);
        text_phone_number= root.findViewById(R.id.clientes_dialog_phone_number);
        text_address= root.findViewById(R.id.clientes_dialog_address);
        text_email= root.findViewById(R.id.clientes_dialog_email);
        textview_wait_message = root.findViewById(R.id.clientes_dialog_wait_message);
        this.hideWaitMessage();

        text_firstname.setText(bundle.getString("firstname"));
        text_lastname.setText(bundle.getString("lastname"));
        text_document.setText(bundle.getString("document"));
        text_email.setText(bundle.getString("email"));
        text_phone_number.setText(bundle.getString("phone_number"));
        text_address.setText(bundle.getString("address"));

        btn_go      = root.findViewById(R.id.clientes_dialog_go);
        btn_cancel  = root.findViewById(R.id.clientes_dialog_cancel);
        final int action;
        action = bundle.getInt("new",1);
        if(action==1){
            text_title.setText("Nuevo cliente");
            btn_go.setText(R.string.register);
        }else{
            text_title.setText("Detalles de cliente");
            btn_go.setText(R.string.update);
        }
        btn_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClienteFragmentForm.this.waitMessage(1);

                HashMap<String,String> data = new HashMap<String,String>();
                data.put("firstname",text_firstname.getText().toString());
                data.put("lastname",text_lastname.getText().toString());
                data.put("document",text_document.getText().toString());
                data.put("email",text_email.getText().toString());
                data.put("phone_number",text_phone_number.getText().toString());
                data.put("address",text_address.getText().toString());
                data.put("id",bundle.getString("id"));

                WS_Clientes.insert_or_update(ClienteFragmentForm.this,data);
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClienteFragmentForm.this.getDialog().cancel();
            }
        });



        if(savedInstanceState!=null){
            String firstname = savedInstanceState.getString("firstname","");
            String lastname = savedInstanceState.getString("lastname","");
            String document = savedInstanceState.getString("document","");
            String email= savedInstanceState.getString("email","");
            String phone_number= savedInstanceState.getString("phone_number","");
            String address = savedInstanceState.getString("address","");
            if(!firstname.equals(""))text_firstname.setText(firstname);
            if(!lastname.equals(""))text_lastname.setText(lastname);
            if(!document.equals(""))text_document.setText(document);
            if(!email.equals(""))text_email.setText(email);
            if(!phone_number.equals(""))text_phone_number.setText(phone_number);
            if(!address.equals(""))text_address.setText(address);
        }



        return builder.create();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(text_firstname!=null && text_firstname.getText()!=null){
            outState.putString("firstname", text_firstname.getText().toString());
        }
        if(text_lastname!=null && text_lastname.getText()!=null){
            outState.putString("lastname", text_lastname.getText().toString());
        }
        if(text_document!=null && text_document.getText()!=null){
            outState.putString("document", text_document.getText().toString());
        }
        if(text_email!=null && text_email.getText()!=null){
            outState.putString("email", text_email.getText().toString());
        }
        if(text_phone_number!=null && text_phone_number.getText()!=null){
            outState.putString("phone_number", text_phone_number.getText().toString());
        }
        if(text_address!=null && text_address.getText()!=null){
            outState.putString("address", text_address.getText().toString());
        }
    }

    private void waitMessage(int state) {
        textview_wait_message.setText(R.string.wait_please);
        textview_wait_message.setVisibility(View.VISIBLE);
    }
    private void hideWaitMessage() {
        textview_wait_message.setVisibility(View.GONE);
    }
    public void queryStatus(String status){
        textview_wait_message.setText(status);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = getArguments();
    }
    @Override
    public void onResume() {

        super.onResume();
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
