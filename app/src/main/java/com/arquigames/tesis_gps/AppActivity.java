package com.arquigames.tesis_gps;

import android.Manifest;
import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.arquigames.tesis_gps.services.PingService;
import com.arquigames.tesis_gps.services.RecordService;
import com.arquigames.tesis_gps.webservices.WS_Tracking;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

public class AppActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        LocationListener,
        OnMapReadyCallback,
        MapSettingsFragment.MapFragmentInteraction,
        ServiceConnection {

    public static final String KEY_SHARED_GPS_TRACKING = "result_shared_gps_tracking";
    public static final String HOST_TRACKING_IP = "45.33.29.198";
    public static final int HOST_TRACKING_PORT = 8284;
    private static final int REQUEST_CODE = 0x0002;
    private static final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 0x00001;
    private static final int MY_PERMISSIONS_REQUEST_READ_ACCESS_FINE_LOCATION = 0x00002;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 0x00003;
    private static final int MY_PERMISSIONS_REQUEST_RECORD_AUDIO = 0x00004;
    private static final int MY_PERMISSIONS_REQUEST_CAPTURE_AUDIO_OUTPUT = 0x00005;

    //static final String HOST_WS = "http://192.168.1.8:8000";
    public static final String HOST_WS = "http://tesis-gps.arquigames.pe";
    public static final String HOST_PREFIX = "/api/android";
    public static final String HOST_WS_PREFIX = HOST_WS+HOST_PREFIX;
    public static final String URL_LOGOUT = HOST_WS_PREFIX+"/users/logout";
    public static final String URL_LOGIN = HOST_WS_PREFIX+"/users/login";
    public static final String URL_PRODUCTOS = HOST_WS_PREFIX+"/products";
    public static final String URL_PAQUETES= HOST_WS_PREFIX+"/packages";
    public static final String URL_PAQUETES_PRODUCTOS= HOST_WS_PREFIX+"/packages/products/";
    public static final String URL_DISPOSITIVOS_GPS = HOST_WS_PREFIX+"/gps_devices";
    public static final String URL_CLIENTES= HOST_WS_PREFIX+"/clients";
    public static final String URL_USUARIOS = HOST_WS_PREFIX+"/users";
    public static final String URL_TRACKING = HOST_WS_PREFIX+"/gpstracking";
    public static final String CALL_RECORD_DIRECTORY = "GRABADOR DE LLAMADAS";
    public static final int IDX_USUARIOS = 0;
    public static final int IDX_PRODUCTOS = 1;
    public static final int IDX_DISPOSITIVOS_GPS = 2;
    public static final int IDX_PAQUETES = 3;
    public static final int IDX_CLIENTES = 4;
    public static final int IDX_MAPA = 5;
    public static final String CHANNEL_ID = "ping_service_channel";
    private HashMap<String, String> json;
    public static final String TAG = "APP_AppActivity";
    private View mProgressView;
    private View mListView;
    private String phone_imei;
    private String phone_number;
    private String phone_sim_serial_number;
    private String phone_sim_operator_name;
    private GoogleMap mMap;
    private float mMapZoomLevel = 10.0f;
    MapSettingsFragment mapSettingsFragment;
    private HashMap<String, Marker> markers = new HashMap<>();


    private Handler handler = new Handler();
    private Runnable runnable = null;
    private boolean pingServiceBound = false;
    private PingService pingService = null;
    private Intent intentService = null;

//Start

    public static JSONObject cleanJSONObject(JSONObject obj) {
        Iterator<String> it =  obj.keys();
        while(it.hasNext()){
            String key = it.next();
            String value = null;
            try {
                value = obj.getString(key);
                if(value==null || value.trim().equals("null"))obj.put(key,"");
            } catch (JSONException ignored) {
            }
        }
        return obj;
    }

    @Override
    public void onPause() {

        if(handler!=null && runnable!=null){
            handler.removeCallbacks(runnable);
            runnable = null;
        }

        super.onPause();
    }
    @Override
    public void onResume() {
        super.onResume();
        runnable = new Runnable() {
            @Override
            public void run() {
                queryDataServer();
                handler.postDelayed(this, 4000);
            }
        };
        handler.postDelayed(runnable, 3000);
        WS_Tracking.try_reload_list = true;
        WS_Tracking.try_reload_list_for_service = true;

    }

    private void queryDataServer() {
        /*
        String date = LoginActivity.getSharedStringValue(this,MapSettingsFragment.GPS_TRACKING_DATE);
        WS_Tracking.list(this,date);
        */
        if(pingServiceBound){
            this.renderMapPoints(pingService.getContent());
        }
    }
    @Override
    public void onDestroy() {
        AppActivity.disableLocationService(this);
        unbindService(this);
        stopService(intentService);
        super.onDestroy();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.maps);
        mapFragment.getMapAsync(this);

        FloatingActionButton map_settings = findViewById(R.id.map_settings_popup);
        map_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mapSettingsFragment==null){
                    mapSettingsFragment = new MapSettingsFragment();
                    mapSettingsFragment.setArguments(null);
                    mapSettingsFragment.show(getSupportFragmentManager(), "MapSettingsFragment");
                    mapSettingsFragment.setCancelable(false);
                }
            }
        });


        json = LoginActivity.getUserInfo(this);
        if(json==null){
            Intent intent = new Intent(AppActivity.this,LoginActivity.class);
            startActivity(intent);
            finish();
        }else{
            TextView textView = headerView.findViewById(R.id.navHeaderNameView);
            textView.setText(json.get("name"));
            textView = headerView.findViewById(R.id.navHeaderEmailView);
            textView.setText(json.get("email"));
            this.createUserMenu(navigationView.getMenu(), json.get("role"));
        }

        this.checkPhoneStatePermission();
        this.checkLocationPermission();
        this.checkWriteExternalStoragePermission();
        this.checkRecordAudioPermission();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            this.checkCaptureAudioOutputPermission();
        }
        this.getPhoneInfo();
        AppActivity.enableLocationService(this);
        intentService = new Intent(this, PingService.class);
        startService(intentService);
        bindService(intentService,this,Context.BIND_AUTO_CREATE);
        createNotificationChannel();
    }


    public void closeMapSettingsFragment(){
        if(mapSettingsFragment!=null){
            mapSettingsFragment.dismiss();
            mapSettingsFragment = null;
        }
    }

    private void createUserMenu(Menu menu, String role) {
        menu.clear();
        if(role.equals("ADMINISTRADOR")){
            menu.add(0, IDX_USUARIOS,0,"Usuarios");
            menu.add(0, IDX_PRODUCTOS,0,"Productos");
            menu.add(0, IDX_DISPOSITIVOS_GPS,0,"Dispositivos GPS");
            menu.add(0, IDX_PAQUETES,0,"Paquetes");
            menu.add(0, IDX_CLIENTES,0,"Clientes");
            menu.getItem(0).setIcon(R.drawable.ic_people_black_24dp);
            menu.getItem(1).setIcon(R.drawable.ic_branding_watermark_black_24dp);
            menu.getItem(2).setIcon(R.drawable.ic_sim_card_black_24dp);
            menu.getItem(3).setIcon(R.drawable.ic_assignment_black_24dp);
            menu.getItem(4).setIcon(R.drawable.ic_people_black_24dp);

        }else if(role.equals("JEFE DE ALMACEN")){
            menu.add(0,IDX_DISPOSITIVOS_GPS,0,"Dispositivos GPS");
            menu.add(0,IDX_PAQUETES,0,"Paquetes");
            menu.getItem(0).setIcon(R.drawable.ic_sim_card_black_24dp);
            menu.getItem(1).setIcon(R.drawable.ic_assignment_black_24dp);

        }else if(role.equals("PERSONAL DE REPARTO")){
            menu.add(0,IDX_PAQUETES,0,"Paquetes");
            menu.getItem(0).setIcon(R.drawable.ic_assignment_black_24dp);

        }else{

        }
    }

    private boolean isServiceRunning(Class serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        if (manager != null) {
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    Log.e (TAG, "Service Running");
                    return true;
                }
            }
        }
        Log.e (TAG, "Service NOT Running");
        return false;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (REQUEST_CODE == requestCode) {
            Intent intent = new Intent(this, RecordService.class);
            startService(intent);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.app, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            this.showInfo();
            return true;
        }
        if (id == R.id.action_logout) {

            LoginActivity.logout(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if(id == IDX_USUARIOS){
            this.openUsuarios(null);
        }else if(id==IDX_PRODUCTOS){
            this.openProductos(null);
        }else if(id==IDX_PAQUETES){
            this.openPaquetes(null);
        }else if(id==IDX_CLIENTES){
            this.openClientes(null);
        }else if(id==IDX_DISPOSITIVOS_GPS){
            this.openDispositivosGps(null);
        }else if(id==IDX_MAPA){
            this.openMapa(null);
        }else{

        }

        /*
        if (id == R.id.nav_cotizaciones) {
            // Handle the camera action
            this.showNoticeCotizaciones();
        } else if (id == R.id.nav_clientes) {

        } else if (id == R.id.nav_polizas) {
            this.showNoticePolizas();

        } else if (id == R.id.nav_cobranzas) {

        }else{

        }
        */

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    private void showInfo(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("App: APP\nDesarrollador: Erick Avalos")
                .setTitle("APP v1.0");
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void getPhoneInfo() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (tm != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                phone_imei      = tm.getImei();
                phone_number    = tm.getLine1Number();
                phone_sim_serial_number = tm.getSimSerialNumber();
                phone_sim_operator_name = tm.getSimOperatorName();
            }else{
                phone_imei      = tm.getDeviceId();
                phone_number    = tm.getLine1Number();
                phone_sim_serial_number = tm.getSimSerialNumber();
                phone_sim_operator_name = tm.getSimOperatorName();
            }
        }
    }
    public static boolean hasPhoneStatePermission(Context ctx){
        return ActivityCompat.checkSelfPermission(ctx, Manifest.permission.READ_PHONE_STATE)== PackageManager.PERMISSION_GRANTED;
    }
    public static boolean hasLocationPermission(Context ctx){
        return ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED;
    }
    public static boolean hasWriteExternalStoragePermission(Context ctx){
        return ActivityCompat.checkSelfPermission(ctx, Manifest.permission.WRITE_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED;
    }
    public static boolean hasRecordAudioPermission(Context ctx){
        return ActivityCompat.checkSelfPermission(ctx, Manifest.permission.RECORD_AUDIO)== PackageManager.PERMISSION_GRANTED;
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static boolean hasCaptureAudioOutputPermission(Context ctx){
        return ActivityCompat.checkSelfPermission(ctx, Manifest.permission.CAPTURE_AUDIO_OUTPUT)== PackageManager.PERMISSION_GRANTED;
    }
    private void checkPhoneStatePermission(){
        boolean granted;
        granted = ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)== PackageManager.PERMISSION_GRANTED;
        if(!granted){
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_PHONE_STATE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_PHONE_STATE},
                        MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
            }
        }

    }
    private void checkLocationPermission(){
        boolean granted;
        granted = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED;
        if(!granted){
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_READ_ACCESS_FINE_LOCATION);
            }
        }

    }
    private void checkWriteExternalStoragePermission(){
        boolean granted;
        granted = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED;
        if(!granted){
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
            }
        }

    }
    private void checkRecordAudioPermission(){
        boolean granted;
        granted = ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)== PackageManager.PERMISSION_GRANTED;
        if(!granted){
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.RECORD_AUDIO)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.RECORD_AUDIO},
                        MY_PERMISSIONS_REQUEST_RECORD_AUDIO);
            }
        }

    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void checkCaptureAudioOutputPermission(){
        boolean granted;
        granted = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAPTURE_AUDIO_OUTPUT)== PackageManager.PERMISSION_GRANTED;
        if(!granted){
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CAPTURE_AUDIO_OUTPUT)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAPTURE_AUDIO_OUTPUT},
                        MY_PERMISSIONS_REQUEST_CAPTURE_AUDIO_OUTPUT);
            }
        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_PHONE_STATE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                }
                break;
            case MY_PERMISSIONS_REQUEST_READ_ACCESS_FINE_LOCATION:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                }
                break;
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                }
                break;
            case MY_PERMISSIONS_REQUEST_RECORD_AUDIO:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                }
                break;

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public static void enableLocationService(Context ctx) {
        if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        // Acquire a reference to the system Location Manager
        LocationManager locationManager = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);

        // Define a listener that responds to location updates
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1500, 3, (LocationListener) ctx);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1500, 3, (LocationListener) ctx);
    }
    public static void disableLocationService(Context ctx){
        LocationManager locationManager = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);
        locationManager.removeUpdates((LocationListener) ctx);
    }

    public void onLocationChanged(Location location) {
        // Called when a new location is found by the network location provider.
        //Log.e(TAG,"onLocationChanged, location"+location.toString());
        try {
            JSONObject json = new JSONObject();
            json.put("lat",location.getLatitude());
            json.put("ln",location.getLongitude());
            json.put("imei",phone_imei);
            json.put("phone_number",phone_number);
            json.put("phone_sim_operator_name",phone_sim_operator_name);
            json.put("phone_sim_serial_number",phone_sim_serial_number);
            Log.e(TAG, String.format("onLocationChanged : %s", json.toString()));
            WS_Tracking.insertWithSocker(this,json);
        } catch (JSONException e) {
            Log.e(TAG,LoginActivity.getStackTrace(e));
        }
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.e(TAG,"onStatusChanged, provider"+provider+", status: "+status+", "+extras.toString());
    }

    public void onProviderEnabled(String provider) {
        Log.e(TAG,"onProviderEnabled, provider"+provider);
    }

    public void onProviderDisabled(String provider) {
        Log.e(TAG,"onProviderDisabled, provider"+provider);
    }

    public void openUsuarios(View view) {

        String source = LoginActivity.getSharedStringValue(this, com.arquigames.tesis_gps.usuarios.UsuariosActivity.KEY_SHARED_PREFERENCE);
        Intent intent = new Intent(AppActivity.this, com.arquigames.tesis_gps.usuarios.UsuariosActivity.class);
        Log.e(TAG,"openUsuarios => "+source);
        intent.putExtra("result", source);
        startActivity(intent);
    }

    public void openProductos(View view) {

        String source = LoginActivity.getSharedStringValue(this, com.arquigames.tesis_gps.productos.ProductosActivity.KEY_SHARED_PREFERENCE);
        Intent intent = new Intent(AppActivity.this, com.arquigames.tesis_gps.productos.ProductosActivity.class);
        Log.e(TAG,"openProductos => "+source);
        intent.putExtra("result", source);
        startActivity(intent);
    }

    public void openPaquetes(View view) {

        String source = LoginActivity.getSharedStringValue(this, com.arquigames.tesis_gps.paquetes.PaquetesActivity.KEY_SHARED_PREFERENCE);
        Intent intent = new Intent(AppActivity.this, com.arquigames.tesis_gps.paquetes.PaquetesActivity.class);
        Log.e(TAG,"openPaquetes => "+source);
        intent.putExtra("result", source);
        startActivity(intent);
    }
    public void openClientes(View view) {

        String source = LoginActivity.getSharedStringValue(this, com.arquigames.tesis_gps.clientes.ClientesActivity.KEY_SHARED_PREFERENCE);
        Intent intent = new Intent(AppActivity.this, com.arquigames.tesis_gps.clientes.ClientesActivity.class);
        Log.e(TAG,"openClientes => "+source);
        intent.putExtra("result", source);
        startActivity(intent);
    }

    public void openDispositivosGps(View view) {

        String source = LoginActivity.getSharedStringValue(this, com.arquigames.tesis_gps.dispositivos_gps.DispositivosGpsActivity.KEY_SHARED_PREFERENCE);
        Intent intent = new Intent(AppActivity.this, com.arquigames.tesis_gps.dispositivos_gps.DispositivosGpsActivity.class);
        Log.e(TAG,"openDispositivosGps => "+source);
        intent.putExtra("result", source);
        startActivity(intent);
    }

    private void openMapa(View view) {
        Intent intent = new Intent(AppActivity.this, com.arquigames.tesis_gps.MapsActivity.class);
        startActivity(intent);

    }
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        LatLng sydney = new LatLng(-12.2139415, -76.9408851);
        MarkerOptions markerOptions = new MarkerOptions().position(sydney).title("Business");
        mMap.clear();
        markers.put("1",mMap.addMarker(markerOptions));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(mMapZoomLevel));
    }

    @Override
    public void onMapDialogClosing(Bundle bundle) {
        this.closeMapSettingsFragment();
    }

    public synchronized void renderMapPoints(JSONObject json) {
        Marker marker;
        if(json==null){
            HashSet<String> keysToRemove = new HashSet<>();
            for (String s : markers.keySet()) {
                if(s.equals("1"))continue;
                marker = markers.get(s);
                keysToRemove.add(s);
                marker.remove();
            }
            for (String s : keysToRemove) {
                markers.remove(s);
            }
            return;
        }
        Iterator<String> it = json.keys();
        MarkerOptions markerOptions;
        LatLng latln;
        try {
            while(it.hasNext()){
                String imei = it.next();
                JSONArray data_imei = json.getJSONArray(imei);
                for (int i = 0; i < data_imei.length(); i++) {
                    JSONObject data_imei_pos = data_imei.getJSONObject(i);
                    String id = data_imei_pos.getString("_id");
                    String key = String.format("%s-%s", imei, id);
                    if (!markers.containsKey(key)) {
                        latln = new LatLng(data_imei_pos.getDouble("lat"), data_imei_pos.getDouble("ln"));
                        markerOptions = new MarkerOptions().position(latln).title(imei + " - " + data_imei_pos.getString("timestamp"));
                        markers.put(key, mMap.addMarker(markerOptions));
                    } else {
                        marker = markers.get(key);
                        latln = new LatLng(data_imei_pos.getDouble("lat"), data_imei_pos.getDouble("ln"));
                        marker.setPosition(latln);
                    }

                }
            }
        } catch (Exception e) {
            Log.e(TAG,LoginActivity.getStackTrace(e));
        }
        //Log.e(TAG, String.format("markers = %d", markers.size()));
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder binder) {
        pingServiceBound = true;
        PingService.LocalBinder mb = (PingService.LocalBinder)binder;
        pingService = mb.getService();
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        pingServiceBound = false;
    }

    @Override
    public void onBindingDied(ComponentName name) {

    }
    private void createNotificationChannel(){
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            if(notificationManager!=null)notificationManager.createNotificationChannel(channel);
        }
    }
}
