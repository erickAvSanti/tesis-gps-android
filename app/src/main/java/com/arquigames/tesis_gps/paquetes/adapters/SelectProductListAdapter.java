package com.arquigames.tesis_gps.paquetes.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.arquigames.tesis_gps.LoginActivity;
import com.arquigames.tesis_gps.R;
import com.arquigames.tesis_gps.paquetes.ui.main.PaqueteFragmentSelectProduct;
import com.arquigames.tesis_gps.paquetes.ui.main.PaqueteFragmentSelectProductTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

public class SelectProductListAdapter extends ArrayAdapter<JSONObject> {
    private static final String TAG = "SelProdListAdapter";
    private PaqueteFragmentSelectProduct ctx;
    private int resource;
    private HashMap<String, JSONObject> mapObjects = new HashMap<>();

    public SelectProductListAdapter(PaqueteFragmentSelectProduct context, int textViewResourceId,
                                    List<JSONObject> objects) {
        super(context.getCtx(), textViewResourceId, objects);
        ctx = context;
        resource = textViewResourceId;
        for (JSONObject obj : objects) {
            try {
                String id = obj.getString("id");
                Log.e("Init ID", id);
                mapObjects.put(id, obj);
            } catch (JSONException e) {
                Log.e(TAG, LoginActivity.getStackTrace(e));
            } catch (Exception e) {
                Log.e(TAG, LoginActivity.getStackTrace(e));
            }
        }
    }

    @Override
    public long getItemId(int position) {
        JSONObject item = getItem(position);
        int id = 0;
        try {
            id = Integer.parseInt(item.getString("id"));
        } catch (JSONException e) {
            Log.e(TAG,LoginActivity.getStackTrace(e));
        }catch (Exception e){
            Log.e(TAG,LoginActivity.getStackTrace(e));
        }
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(ctx.getCtx());
            v = vi.inflate(resource, null);
        }

        JSONObject data = getItem(position);

        if (data != null) {
            PaqueteFragmentSelectProductTextView name = v.findViewById(R.id.paquetes_select_product_list_item_name);
            try {
                name.setText(String.format("%s - %s", data.getString("name"), data.getString("sku")));
                name.setProduct_id(data.getString("id"));
                name.setProduct(String.format("%s %s", data.getString("name"), data.getString("sku")));
                name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PaqueteFragmentSelectProductTextView tt = (PaqueteFragmentSelectProductTextView)v;
                        Log.e(TAG,String.format("Select productID = %s and quantity= %s",tt.getProduct_id(),ctx.getQuantity()));
                        ctx.getPaqueteFragmentProductos().setSelectedProduct(mapObjects.get(tt.getProduct_id()),ctx.getQuantity());
                    }
                });
            } catch (JSONException e) {
                Log.e(TAG,LoginActivity.getStackTrace(e));
                if(name!=null){
                    name.setText("Error");
                }
            }
            catch (Exception e){
                Log.e(TAG,LoginActivity.getStackTrace(e));
            }
        }else{
            Log.e(TAG, String.format("Data is null at %d", position));
        }

        return v;
    }


    @Override
    public boolean hasStableIds() {
        return true;
    }
}
