package com.arquigames.tesis_gps.paquetes;

import android.content.Context;
import android.util.AttributeSet;

public class PaqueteNameTextView extends android.support.v7.widget.AppCompatTextView {
    private String paquete_id;
    public PaqueteNameTextView(Context context) {
        super(context);
    }

    public PaqueteNameTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PaqueteNameTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public String getPaquete_id() {
        return paquete_id;
    }

    public void setPaquete_id(String paquete_id) {
        this.paquete_id = paquete_id;
    }
}
