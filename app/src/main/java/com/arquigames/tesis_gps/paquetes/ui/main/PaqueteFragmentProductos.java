package com.arquigames.tesis_gps.paquetes.ui.main;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.arquigames.tesis_gps.AppActivity;
import com.arquigames.tesis_gps.LoginActivity;
import com.arquigames.tesis_gps.R;
import com.arquigames.tesis_gps.paquetes.PaquetesActivity;
import com.arquigames.tesis_gps.paquetes.adapters.PaquetesProductosListAdapter;
import com.arquigames.tesis_gps.webservices.WS_Paquetes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PaqueteFragmentProductos extends Fragment {

    private static final String TAG = "PaqFragmentProductos";
    private PaqueteFragmentProductosViewModel mViewModel;

    private AppCompatTextView textview_wait_message;
    private AppCompatEditText name;
    private AppCompatEditText description;
    private AppCompatTextView client;
    private AppCompatTextView gps_device;
    private AppCompatTextView user;
    private View root;
    private AppCompatActivity ctx;
    private PaqueteFragmentSelectProduct paqueteFragmentSelectProduct;


    private Handler handler = new Handler();
    private ArrayList<String> list;
    private HashMap<String,JSONObject> list_map;
    private PaquetesProductosListAdapter productoAdapter;
    private ListView swipe_list;
    private SwipeRefreshLayout swipe;

    private OnPaqueteFragmentProductosInteractionListener mListener;

    private Bundle bundle;
    private HashMap<String, String> list_map_quantity;

    public static PaqueteFragmentProductos newInstance(AppCompatActivity ctx, Intent extras) {
        PaqueteFragmentProductos fragment = new PaqueteFragmentProductos();
        if(extras!=null)fragment.setArguments(extras.getExtras());
        fragment.setCtx(ctx);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(PaqueteFragmentProductosViewModel.class);
        bundle = getArguments();
        if ( bundle== null)bundle = new Bundle();
        mViewModel.setId(bundle.getString("id"));
        mViewModel.setName(bundle.getString("name"));
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_paquete_detail_products, container, false);
        FloatingActionButton fab = root.findViewById(R.id.fab_add_product);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PaqueteFragmentProductos.this.openSelectProduct();

            }
        });



        swipe = root.findViewById(R.id.swipe_paquetes_productos);
        swipe_list = root.findViewById(R.id.swipe_paquetes_productos_list);

        swipe.setColorSchemeResources(R.color.color_swipe_list_foreground);
        swipe.setProgressBackgroundColorSchemeResource(R.color.color_swipe_list_background);

        String source = LoginActivity.getSharedStringValue(this.getActivity(), PaquetesActivity.KEY_SHARED_PREFERENCE);
        JSONArray arr = new JSONArray();
        boolean pass = false;
        if(source.isEmpty()){
            this.getList();
        }else{
            try {
                Log.e(TAG,"SOURCE => "+source);
                arr = new JSONArray(source);
                for(int i = 0 ; i<arr.length();i++){
                    JSONObject obj = arr.getJSONObject(i);
                    String id = obj.getString("id");
                    if(id.equals(mViewModel.getId().getValue())){
                        if(obj.has("products")){
                            Object tmp = obj.get("products");
                            if(tmp instanceof JSONArray){
                                arr = (JSONArray)tmp;
                                pass = true;
                                break;
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                Log.e(TAG,LoginActivity.getStackTrace(e));
            }catch (Exception e) {
                Log.e(TAG,LoginActivity.getStackTrace(e));
            }
        }
        if(!pass)arr = new JSONArray();
        SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                PaqueteFragmentProductos.this.getList();
            }
        };
        swipe.setOnRefreshListener(onRefreshListener);
        this.fillList(arr);


        ;
        return root;
    }

    public void getList() {
        if(mListener!=null){
            mListener.clearRemoveProducto();
        }
        handler.post(new Runnable() {
            @Override
            public void run() {
                PaqueteFragmentProductos.this.getData();
            }
        });
    }

    public void fillList(JSONArray arr){
        List<JSONObject> values = new ArrayList<>();
        if(list_map!=null){
            list_map.clear();
            list_map_quantity.clear();
        }else{
            list_map = new HashMap<>();
            list_map_quantity = new HashMap<>();
        }
        if(arr.length()>0){
            for(int i=0;i<arr.length();i++){
                try {
                    JSONObject obj = arr.getJSONObject(i);
                    obj = AppActivity.cleanJSONObject(obj);
                    values.add(obj);
                    String id = obj.getString("id");
                    list_map.put(id,obj);
                    boolean flag = false;
                    if(obj.has("pivot")){
                        Object tmp = obj.get("pivot");
                        if(tmp instanceof JSONObject){
                            JSONObject pivot = (JSONObject)tmp;
                            list_map_quantity.put(id,pivot.getString("quantity"));
                            flag = true;
                        }
                    }
                    if(!flag){
                        list_map_quantity.put(id,"1");
                    }
                } catch (JSONException e) {
                    Log.e(TAG,LoginActivity.getStackTrace(e));
                }
            }
        }
        this.putList(values);
    }
    private void putProduct(JSONObject obj, String quantity){
        List<JSONObject> values = new ArrayList<>();
        try {
            String id = obj.getString("id");
            list_map.put(id,obj);
            list_map_quantity.put(id,quantity);
            for (String s : list_map.keySet()) {
                values.add(list_map.get(s));
            }
        } catch (Exception e) {
            Log.e(TAG,LoginActivity.getStackTrace(e));
        }
        this.putList(values);
    }

    private void putList(List<JSONObject> values) {
        if(mListener!=null){
            mListener.setProductos(values,list_map_quantity);
        }
        productoAdapter = new PaquetesProductosListAdapter(this, R.layout.paquetes_productos_list_item, values,list_map_quantity);
        swipe_list.setAdapter(productoAdapter);
        notifySwipeAdapter();
    }

    private void getData(){
        WS_Paquetes.reload_productos(this,mViewModel.getId().getValue());
    }

    public void stopSwipeRefreshing(){
        swipe.setRefreshing(false);
    }
    public void notifySwipeAdapter(){
        stopSwipeRefreshing();
        productoAdapter.notifyDataSetChanged();
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // TODO: Use the ViewModel
    }
    @Override
    public void onResume() {

        super.onResume();

    }

    private void openSelectProduct() {


        paqueteFragmentSelectProduct = new PaqueteFragmentSelectProduct();
        paqueteFragmentSelectProduct.setPaqueteFragmentProductos(this);
        Bundle bundle = new Bundle();
        paqueteFragmentSelectProduct.setArguments(bundle);
        paqueteFragmentSelectProduct.setCtx(this.ctx);
        paqueteFragmentSelectProduct.show(getFragmentManager(), "PaqueteFragmentSelectProduct");

    }

    public Context getCtx() {
        return ctx;
    }

    public void setCtx(AppCompatActivity ctx) {
        this.ctx = ctx;
    }

    public void setSelectedProduct(JSONObject data, String quantity) {
        putProduct(data,quantity);
        if(paqueteFragmentSelectProduct!=null){
            paqueteFragmentSelectProduct.dismiss();
            paqueteFragmentSelectProduct = null;
        }
    }
    public void openRemovePaqueteProducto(String id) {

        JSONObject info = list_map.get(id);
        Bundle bundle = this.getBundleProducto(info);
        if(bundle.size()>0){
            PaqueteProductoFragmentDelete dialog = new PaqueteProductoFragmentDelete();
            dialog.setArguments(bundle);
            dialog.setPaqueteFragmentProductos(this);
            dialog.show(this.getFragmentManager(), "PaqueteProductoFragmentDelete");
        }
    }
    private Bundle getBundleProducto(JSONObject info) {
        JSONObject extra;
        Object tmp;
        Bundle bundle = new Bundle();
        try {
            bundle.putString("name", info.getString("name"));
            bundle.putString("sku", info.getString("sku"));
            bundle.putString("description", info.getString("description"));
            bundle.putString("id", info.getString("id"));
        }catch(Exception e){
            Log.e(TAG,LoginActivity.getStackTrace(e));
        }
        return bundle;
    }

    public void removeProducto(String id) {
        list_map.remove(id);
        list_map_quantity.remove(id);
        List<JSONObject> values = new ArrayList<>();
        for (String s : list_map.keySet()) {
            JSONObject data = list_map.get(s);
            values.add(data);
        }
        if(mListener!=null){
            mListener.removeProducto(id);
        }
        this.putList(values);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnPaqueteFragmentProductosInteractionListener) {
            mListener = (OnPaqueteFragmentProductosInteractionListener) context;
        }
    }

    public interface OnPaqueteFragmentProductosInteractionListener {
        // TODO: Update argument type and name
        void removeProducto(String id);
        void clearRemoveProducto();
        void setProductos(List<JSONObject> ids, HashMap<String, String> quantity);
    }
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
