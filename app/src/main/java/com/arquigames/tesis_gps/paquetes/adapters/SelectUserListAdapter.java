package com.arquigames.tesis_gps.paquetes.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.arquigames.tesis_gps.LoginActivity;
import com.arquigames.tesis_gps.R;
import com.arquigames.tesis_gps.paquetes.PaqueteDetailActivity;
import com.arquigames.tesis_gps.paquetes.ui.main.PaqueteFragmentSelectUser;
import com.arquigames.tesis_gps.paquetes.ui.main.PaqueteFragmentSelectUserTextView;
import com.arquigames.tesis_gps.paquetes.ui.main.PaqueteFragmentSelectUserTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

public class SelectUserListAdapter extends ArrayAdapter<JSONObject> {
    private static final String TAG = "SelUserListAdapter";
    private PaqueteFragmentSelectUser ctx;
    private int resource;
    private HashMap<String, JSONObject> mapObjects = new HashMap<>();

    public SelectUserListAdapter(PaqueteFragmentSelectUser context, int textViewResourceId,
                                      List<JSONObject> objects) {
        super(context.getCtx(), textViewResourceId, objects);
        ctx = context;
        resource = textViewResourceId;
        for (JSONObject obj : objects) {
            try {
                String id = obj.getString("id");
                Log.e("Init ID", id);
                mapObjects.put(id, obj);
            } catch (JSONException e) {
                Log.e(TAG, LoginActivity.getStackTrace(e));
            } catch (Exception e) {
                Log.e(TAG, LoginActivity.getStackTrace(e));
            }
        }
    }

    @Override
    public long getItemId(int position) {
        JSONObject item = getItem(position);
        int id = 0;
        try {
            id = Integer.parseInt(item.getString("id"));
        } catch (JSONException e) {
            Log.e(TAG,LoginActivity.getStackTrace(e));
        }catch (Exception e){
            Log.e(TAG,LoginActivity.getStackTrace(e));
        }
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(ctx.getCtx());
            v = vi.inflate(resource, null);
        }

        JSONObject data = getItem(position);

        if (data != null) {
            PaqueteFragmentSelectUserTextView name = v.findViewById(R.id.paquetes_select_user_list_item_name);
            try {
                name.setText(String.format("%s", data.getString("name")));
                name.setUser_id(data.getString("id"));
                name.setUser(String.format("%s", data.getString("name")));
                name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PaqueteFragmentSelectUserTextView tt = (PaqueteFragmentSelectUserTextView)v;
                        ctx.getPaqueteFragmentPrincipal().setSelectedUser(tt.getUser_id(),tt.getUser());
                    }
                });
            } catch (JSONException e) {
                Log.e(TAG,LoginActivity.getStackTrace(e));
                if(name!=null){
                    name.setText("Error");
                }
            }
            catch (Exception e){
                Log.e(TAG,LoginActivity.getStackTrace(e));
            }
        }else{
            Log.e(TAG, String.format("Data is null at %d", position));
        }

        return v;
    }


    @Override
    public boolean hasStableIds() {
        return true;
    }
}
