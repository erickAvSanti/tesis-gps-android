package com.arquigames.tesis_gps.paquetes.ui.main;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

public class PaqueteFragmentPrincipalViewModel extends ViewModel {
    // TODO: Implement the ViewModel
    private MutableLiveData<String> id              = new MutableLiveData<>();
    private MutableLiveData<String> name            = new MutableLiveData<>();
    private MutableLiveData<String> clients_id      = new MutableLiveData<>();
    private MutableLiveData<String> client          = new MutableLiveData<>();
    private MutableLiveData<String> gps_devices_id  = new MutableLiveData<>();
    private MutableLiveData<String> gps_device      = new MutableLiveData<>();
    private MutableLiveData<String> users_id        = new MutableLiveData<>();
    private MutableLiveData<String> user            = new MutableLiveData<>();
    private MutableLiveData<String> delivery_day    = new MutableLiveData<>();
    private MutableLiveData<String> delivered       = new MutableLiveData<>();
    private MutableLiveData<String> delivered_at    = new MutableLiveData<>();
    private MutableLiveData<String> description     = new MutableLiveData<>();

    public void setId(String value) {
        id.setValue(value);
    }
    public LiveData<String> getId() {
        return id;
    }
    public void setName(String value) {
        name.setValue(value);
    }
    public LiveData<String> getName() {
        return name;
    }
    public void setClients_id(String value) {
        if(value==null)value="";
        clients_id.setValue(value);
    }
    public LiveData<String> getClients_id() {
        return clients_id;
    }
    public void setClient(String value) {
        if(value==null || value.equals("") || value.equals("null"))value="Seleccionar Cliente";
        client.setValue(value);
    }
    public LiveData<String> getClient() {
        return client;
    }
    public void setUser(String value) {
        if(value==null || value.equals("") || value.equals("null"))value="Usuario no asignado";
        user.setValue(value);
    }
    public LiveData<String> getUser() {
        return user;
    }
    public void setGps_device(String value) {
        if(value==null || value.equals("") || value.equals("null"))value="Seleccionar dispositivo GPS";
        gps_device.setValue(value);
    }
    public LiveData<String> getGps_device() {
        return gps_device;
    }
    public void setGps_devices_id(String value) {
        if(value==null)value="";
        gps_devices_id.setValue(value);
    }
    public LiveData<String> getGps_devices_id() {
        return gps_devices_id;
    }
    public void setUsers_id(String value) {
        if(value==null)value="";
        users_id.setValue(value);
    }
    public LiveData<String> getUsers_id() {
        return users_id;
    }
    public void setDelivered(String value) {
        if(value==null)value="";
        delivered.setValue(value);
    }
    public LiveData<String> getDelivered() {
        return delivered;
    }
    public void setDelivered_at(String value) {
        if(value==null)value="";
        delivered_at.setValue(value);
    }
    public LiveData<String> getDelivered_at() {
        return delivered_at;
    }
    public void setDescription(String value) {
        if(value==null)value="";
        description.setValue(value);
    }
    public LiveData<String> getDescription() {
        return description;
    }
    public void setDelivery_day(String value) {
        if(value==null)value="";
        delivery_day.setValue(value);
    }
    public LiveData<String> getDelivery_day() {
        return delivery_day;
    }
}
