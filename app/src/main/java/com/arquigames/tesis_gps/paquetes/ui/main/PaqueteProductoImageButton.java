package com.arquigames.tesis_gps.paquetes.ui.main;

import android.content.Context;
import android.support.v7.widget.AppCompatImageButton;
import android.util.AttributeSet;

public class PaqueteProductoImageButton extends AppCompatImageButton {
    private String producto_delete_id;
    public PaqueteProductoImageButton(Context context) {
        super(context);
    }

    public PaqueteProductoImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PaqueteProductoImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public String getProducto_delete_id() {
        return producto_delete_id;
    }

    public void setProducto_delete_id(String producto_delete_id) {
        this.producto_delete_id = producto_delete_id;
    }
}
