package com.arquigames.tesis_gps.paquetes.ui.main;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

public class PaqueteFragmentProductosViewModel extends ViewModel {
    // TODO: Implement the ViewModel
    private MutableLiveData<String> id              = new MutableLiveData<>();
    private MutableLiveData<String> name            = new MutableLiveData<>();

    public void setId(String value) {
        id.setValue(value);
    }
    public LiveData<String> getId() {
        return id;
    }

    public void setName(String value) {
        name.setValue(value);
    }
    public LiveData<String> getName() {
        return name;
    }
}
