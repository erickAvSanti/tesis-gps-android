package com.arquigames.tesis_gps.paquetes.ui.main;

import android.content.Context;
import android.util.AttributeSet;

public class PaqueteFragmentSelectProductTextView extends android.support.v7.widget.AppCompatTextView {
    private String product_id;
    private String product;
    public PaqueteFragmentSelectProductTextView(Context context) {
        super(context);
    }

    public PaqueteFragmentSelectProductTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PaqueteFragmentSelectProductTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }
}
