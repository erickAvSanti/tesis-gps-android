package com.arquigames.tesis_gps.paquetes.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.arquigames.tesis_gps.LoginActivity;
import com.arquigames.tesis_gps.R;
import com.arquigames.tesis_gps.paquetes.ui.main.PaqueteFragmentProductos;
import com.arquigames.tesis_gps.paquetes.ui.main.PaqueteProductoImageButton;
import com.arquigames.tesis_gps.paquetes.ui.main.PaqueteProductoNameTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

public class PaquetesProductosListAdapter extends ArrayAdapter<JSONObject> {
    private static final String TAG = "UsuariosListAdapter";
    private PaqueteFragmentProductos ctx;
    private int resource;
    private HashMap<String, JSONObject> mapObjects = new HashMap<>();
    private HashMap<String, String> mapQuantity = new HashMap<>();

    public PaquetesProductosListAdapter(PaqueteFragmentProductos context, int textViewResourceId,
                                        List<JSONObject> objects, HashMap<String, String> quantity) {
        super(context.getActivity(), textViewResourceId, objects);
        ctx = context;
        mapQuantity = quantity;
        resource = textViewResourceId;
        for (int i = 0; i < objects.size(); ++i) {
            JSONObject obj = objects.get(i);
            try {
                String id = obj.getString("id");
                Log.e("Init ID",id);
                mapObjects.put(id,obj);
            } catch (JSONException e) {
                Log.e(TAG, LoginActivity.getStackTrace(e));
            }
        }
    }

    @Override
    public long getItemId(int position) {
        JSONObject item = getItem(position);
        int id = 0;
        try {
            id = Integer.parseInt(item.getString("id"));
            Log.e("getITemId",String.valueOf(id));
        } catch (JSONException e) {
            Log.e(TAG,LoginActivity.getStackTrace(e));
        }
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(ctx.getCtx());
            v = vi.inflate(resource, null);
        }

        JSONObject data = getItem(position);
        String id="";

        if (data != null) {
            PaqueteProductoNameTextView name = v.findViewById(R.id.paquetes_productos_list_name);
            PaqueteProductoImageButton close = v.findViewById(R.id.paquetes_productos_list_delete);
            try {
                id = data.getString("id");
                String qty = "?";
                boolean pass = false;
                if(data.has("pivot")){
                    Object tmp = data.get("pivot");
                    if(tmp instanceof JSONObject) {
                        JSONObject pivot = (JSONObject) tmp;
                        qty = pivot.getString("quantity");
                        pass = true;
                    }
                }
                if(!pass){
                    qty = this.mapQuantity.get(id);
                    if(qty==null)qty ="?";
                }
                name.setText(String.format("%s) %s - %s [%s]",String.valueOf(position+1), data.getString("name"), data.getString("sku"), qty));
                name.setProducto_id(data.getString("id"));
                close.setProducto_delete_id(id);
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ctx.openRemovePaqueteProducto(((PaqueteProductoImageButton)v).getProducto_delete_id());
                    }
                });
            } catch (JSONException e) {
                Log.e(TAG,LoginActivity.getStackTrace(e));
                name.setText("Error");
            }
        }

        return v;
    }


    @Override
    public boolean hasStableIds() {
        return true;
    }
}
