package com.arquigames.tesis_gps.paquetes.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.arquigames.tesis_gps.LoginActivity;
import com.arquigames.tesis_gps.R;
import com.arquigames.tesis_gps.paquetes.PaqueteImageButton;
import com.arquigames.tesis_gps.paquetes.PaqueteNameTextView;
import com.arquigames.tesis_gps.paquetes.PaquetesActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

public class PaquetesListAdapter extends ArrayAdapter<JSONObject> {
    private static final String TAG = "UsuariosListAdapter";
    private PaquetesActivity ctx;
    private int resource;
    private HashMap<String, JSONObject> mapObjects = new HashMap<String,JSONObject>();

    public PaquetesListAdapter(PaquetesActivity context, int textViewResourceId,
                                List<JSONObject> objects) {
        super(context, textViewResourceId, objects);
        ctx = context;
        resource = textViewResourceId;
        for (int i = 0; i < objects.size(); ++i) {
            JSONObject obj = objects.get(i);
            try {
                String id = obj.getString("id");
                Log.e("Init ID",id);
                mapObjects.put(id,obj);
            } catch (JSONException e) {
                Log.e(TAG, LoginActivity.getStackTrace(e));
            }
        }
    }

    @Override
    public long getItemId(int position) {
        JSONObject item = getItem(position);
        int id = 0;
        try {
            id = Integer.parseInt(item.getString("id"));
            Log.e("getITemId",String.valueOf(id));
        } catch (JSONException e) {
            Log.e(TAG,LoginActivity.getStackTrace(e));
        }
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(ctx);
            v = vi.inflate(resource, null);
        }

        JSONObject data = getItem(position);

        if (data != null) {
            PaqueteNameTextView name = v.findViewById(R.id.paquetes_list_name);
            PaqueteImageButton close = v.findViewById(R.id.paquetes_list_delete);
            try {
                name.setText(String.format("%s", data.getString("name")));
                name.setPaquete_id(data.getString("id"));
                close.setPaquete_delete_id(data.getString("id"));
                name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ctx.editPaquete(((PaqueteNameTextView)v).getPaquete_id());
                    }
                });
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ctx.deletePaquete(((PaqueteImageButton)v).getPaquete_delete_id());
                    }
                });
            } catch (JSONException e) {
                Log.e(TAG,LoginActivity.getStackTrace(e));
                name.setText("Error");
            }
        }

        return v;
    }


    @Override
    public boolean hasStableIds() {
        return true;
    }
}
