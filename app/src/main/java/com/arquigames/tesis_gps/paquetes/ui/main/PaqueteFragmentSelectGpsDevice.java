package com.arquigames.tesis_gps.paquetes.ui.main;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.arquigames.tesis_gps.AppActivity;
import com.arquigames.tesis_gps.LoginActivity;
import com.arquigames.tesis_gps.R;
import com.arquigames.tesis_gps.paquetes.adapters.SelectGpsDeviceListAdapter;
import com.arquigames.tesis_gps.webservices.WS_Clientes;
import com.arquigames.tesis_gps.webservices.WS_DispositivosGps;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PaqueteFragmentSelectGpsDevice extends AppCompatDialogFragment {
    // TODO: Rename parameter arguments, choose names that match
    private static final String TAG = "PaqFragmentSelectClient";

    // TODO: Rename and change types of parameters
    private AppCompatTextView textview_wait_message;

    private OnFragmentInteractionListener mListener;
    private Bundle bundle;
    private TextView text_title;
    private View root;
    private SelectGpsDeviceListAdapter adapter;
    private ListView list;
    private HashMap<String,JSONObject> list_map;
    private AppCompatActivity ctx;
    private PaqueteFragmentPrincipal paqueteFragmentPrincipal;

    public PaqueteFragmentSelectGpsDevice() {
        // Required empty public constructor
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.ctx);
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        root = inflater.inflate(R.layout.dialog_fragment_paquetes_select_gps_device, null);
        builder.setView(root);
        list = root.findViewById(R.id.paquetes_select_gps_device_list);
        String source = LoginActivity.getSharedStringValue(this.ctx, com.arquigames.tesis_gps.dispositivos_gps.DispositivosGpsActivity.KEY_SHARED_PREFERENCE);
        AppCompatImageButton btn = root.findViewById(R.id.paquetes_select_gps_device_list_reload);
        textview_wait_message = root.findViewById(R.id.paquetes_select_gps_device_list_status);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PaqueteFragmentSelectGpsDevice.this.waitMessage();
                WS_DispositivosGps.reload_from_paquetes(PaqueteFragmentSelectGpsDevice.this);
            }
        });
        this.fillList(source);


        return builder.create();
    }

    public void fillList(String source){

        Log.e(TAG, String.format("fill gps devices from %s", source));

        JSONArray arr = new JSONArray();
        try {
            if(source!=null && !source.isEmpty()){
                arr = new JSONArray(source);
            }
        } catch (JSONException e) {
            Log.e(TAG,LoginActivity.getStackTrace(e));
        }
        this.fillList(arr);

    }

    public void fillList(JSONArray arr) {
        List<JSONObject> values = new ArrayList<>();
        if(list_map!=null){
            list_map.clear();
        }else{
            list_map = new HashMap<>();
        }
        if(arr.length()>0){
            for(int i=0;i<arr.length();i++){
                try {
                    JSONObject obj = arr.getJSONObject(i);
                    obj = AppActivity.cleanJSONObject(obj);
                    values.add(obj);
                    list_map.put(obj.getString("id"),obj);
                } catch (JSONException e) {
                    Log.e(TAG,LoginActivity.getStackTrace(e));
                }
            }
        }
        Log.e(TAG, String.format("fill gps devices from LIST %s", values.toString()));
        adapter = new SelectGpsDeviceListAdapter(this, R.layout.dialog_fragment_paquetes_select_gps_device_list_item, values);
        list.setAdapter(adapter);
    }

    private void waitMessage() {
        textview_wait_message.setText(R.string.wait_please);
        textview_wait_message.setVisibility(View.VISIBLE);
    }
    public void hideWaitMessage() {
        textview_wait_message.setVisibility(View.GONE);
    }
    public void queryStatus(String status){
        textview_wait_message.setText(status);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = getArguments();
    }
    @Override
    public void onResume() {

        super.onResume();
        /*
        LinearLayout linearLayout = root.findViewById(R.id.paquetes_select_gps_device_list);
        TextView txt;
        for(int i = 0; i<50;i++){
            txt = new TextView(this.getActivity());
            txt.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT));
            txt.setText(String.format(Locale.ENGLISH,"TextView %d", i));
            linearLayout.addView(txt);
        }
        */
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void setCtx(AppCompatActivity ctx) {
        this.ctx =ctx;
    }
    public AppCompatActivity getCtx() {
        return this.ctx;
    }

    public PaqueteFragmentPrincipal getPaqueteFragmentPrincipal() {
        return paqueteFragmentPrincipal;
    }

    public void setPaqueteFragmentPrincipal(PaqueteFragmentPrincipal paqueteFragmentPrincipal) {
        this.paqueteFragmentPrincipal = paqueteFragmentPrincipal;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Bundle bundle);
    }
}
