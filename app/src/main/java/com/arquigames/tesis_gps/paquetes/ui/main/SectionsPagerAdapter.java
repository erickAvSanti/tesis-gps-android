package com.arquigames.tesis_gps.paquetes.ui.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;

import com.arquigames.tesis_gps.R;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    @StringRes
    private static final int[] TAB_TITLES = new int[]{R.string.title_paquetes_principal, R.string.title_paquetes_productos};
    private final AppCompatActivity mContext;
    private Intent extras;

    public SectionsPagerAdapter(AppCompatActivity context, FragmentManager fm, Intent extras) {
        super(fm);
        mContext = context;
        this.extras = extras;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        if(position==0){
            return PaqueteFragmentPrincipal.newInstance(mContext,extras);
        }else{
            //return PlaceholderFragment.newInstance(position + 1);
            return PaqueteFragmentProductos.newInstance(mContext,extras);
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        // Show 2 total pages.
        return 2;
    }
}