package com.arquigames.tesis_gps.paquetes.ui.main;

import android.content.Context;
import android.util.AttributeSet;

public class PaqueteFragmentSelectClientTextView extends android.support.v7.widget.AppCompatTextView {
    private String client_id;
    private String client;
    public PaqueteFragmentSelectClientTextView(Context context) {
        super(context);
    }

    public PaqueteFragmentSelectClientTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PaqueteFragmentSelectClientTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }
}
