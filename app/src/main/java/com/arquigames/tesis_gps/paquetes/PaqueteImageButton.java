package com.arquigames.tesis_gps.paquetes;

import android.content.Context;
import android.support.v7.widget.AppCompatImageButton;
import android.util.AttributeSet;

public class PaqueteImageButton extends AppCompatImageButton {
    private String paquete_delete_id;
    public PaqueteImageButton(Context context) {
        super(context);
    }

    public PaqueteImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PaqueteImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public String getPaquete_delete_id() {
        return paquete_delete_id;
    }

    public void setPaquete_delete_id(String paquete_delete_id) {
        this.paquete_delete_id = paquete_delete_id;
    }
}
