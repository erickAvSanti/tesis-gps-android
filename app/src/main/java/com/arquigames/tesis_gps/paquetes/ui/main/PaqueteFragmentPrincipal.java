package com.arquigames.tesis_gps.paquetes.ui.main;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ToggleButton;

import com.arquigames.tesis_gps.LoginActivity;
import com.arquigames.tesis_gps.R;
import com.arquigames.tesis_gps.paquetes.PaqueteDetailActivity;
import com.arquigames.tesis_gps.webservices.WS_Paquetes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;

public class PaqueteFragmentPrincipal extends Fragment {

    private static final String TAG = "PaqFragmentPrincipal";
    private PaqueteFragmentPrincipalViewModel mViewModel;

    private AppCompatTextView textview_wait_message;
    private AppCompatEditText name;
    private AppCompatEditText description;
    private AppCompatTextView client;
    private AppCompatTextView gps_device;
    private AppCompatTextView user;
    private ToggleButton delivered;
    private AppCompatTextView delivered_at;
    private View root;
    private AppCompatActivity ctx;
    private PaqueteFragmentSelectClient paqueteFragmentSelectClient;
    private PaqueteFragmentSelectGpsDevice paqueteFragmentSelectGpsDevice;
    private PaqueteFragmentSelectUser paqueteFragmentSelectUser;
    private String _delivered_at="";
    private String _delivered="";
    private String _delivery_day="";
    private DatePicker delivery_day;

    public static PaqueteFragmentPrincipal newInstance(AppCompatActivity ctx, Intent extras) {
        PaqueteFragmentPrincipal fragment = new PaqueteFragmentPrincipal();
        if(extras!=null)fragment.setArguments(extras.getExtras());
        fragment.setCtx(ctx);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(PaqueteFragmentPrincipalViewModel.class);
        Bundle arguments = getArguments();
        if ( arguments== null)arguments = new Bundle();
        mViewModel.setId(arguments.getString("id"));
        mViewModel.setName(arguments.getString("name"));
        mViewModel.setClients_id(arguments.getString("clients_id"));
        mViewModel.setGps_devices_id(arguments.getString("gps_devices_id"));
        mViewModel.setUser(arguments.getString("user"));
        mViewModel.setUsers_id(arguments.getString("users_id"));
        _delivery_day = arguments.getString("delivery_day");
        mViewModel.setDelivery_day(_delivery_day);
        Log.e(TAG, String.format("delivery_day => %s", _delivery_day));
        _delivered = arguments.getString("delivered");
        if(_delivered==null)_delivered="NO";
        mViewModel.setDelivered(_delivered);
        _delivered_at = arguments.getString("delivered_at");
        mViewModel.setDelivered_at(_delivered_at);
        mViewModel.setDescription(arguments.getString("description"));
        mViewModel.setClient(arguments.getString("client"));
        mViewModel.setUser(arguments.getString("user"));
        mViewModel.setGps_device(arguments.getString("gps_device"));
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_paquete_detail_principal, container, false);
        name = root.findViewById(R.id.paquetes_principal_dialog_name);
        description = root.findViewById(R.id.paquetes_principal_dialog_description);
        client      = root.findViewById(R.id.paquetes_principal_dialog_client);
        gps_device  = root.findViewById(R.id.paquetes_principal_dialog_gps_device);
        user        = root.findViewById(R.id.paquetes_principal_dialog_user);
        delivered   = root.findViewById(R.id.paquetes_principal_dialog_delivered);
        delivery_day   = root.findViewById(R.id.paquetes_principal_dialog_delivery_day);
        delivered_at   = root.findViewById(R.id.paquetes_principal_dialog_delivered_at);
        delivered_at.setText(String.format("Última fecha registrada como entregado: %s", _delivered_at));
        mViewModel.getName().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                PaqueteFragmentPrincipal.this.name.setText(s);
            }
        });
        mViewModel.getDescription().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                PaqueteFragmentPrincipal.this.description.setText(s);
            }
        });
        mViewModel.getClient().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                PaqueteFragmentPrincipal.this.client.setText(s);
            }
        });
        mViewModel.getGps_device().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                PaqueteFragmentPrincipal.this.gps_device.setText(s);
            }
        });
        mViewModel.getUser().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                PaqueteFragmentPrincipal.this.user.setText(s);
            }
        });
        mViewModel.getDelivery_day().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                if(s!=null && s!="" && s!="null"){
                    String[] tmp = s.split("-");
                    if(tmp.length==3){
                        Log.e(TAG, String.format("setup datepicker %s %s %s", tmp[0], tmp[1], tmp[2]));
                        PaqueteFragmentPrincipal.this.delivery_day.updateDate(Integer.parseInt(tmp[0]),Integer.parseInt(tmp[1])-1,Integer.parseInt(tmp[2]));
                    }
                }
            }
        });
        AppCompatButton btn = root.findViewById(R.id.paquetes_principal_dialog_go);
        textview_wait_message = root.findViewById(R.id.paquetes_principal_dialog_wait_message);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PaqueteFragmentPrincipal.this.try_form(1);
            }
        });

        return root;
    }

    private void try_form(int i){
        Bundle bundle = new Bundle();
        bundle.putString("id",mViewModel.getId().getValue());
        bundle.putString("name",this.name.getText().toString());
        bundle.putString("clients_id",mViewModel.getClients_id().getValue());
        bundle.putString("client",mViewModel.getClient().getValue());
        bundle.putString("gps_devices_id",mViewModel.getGps_devices_id().getValue());
        bundle.putString("gps_device",mViewModel.getGps_device().getValue());
        bundle.putString("users_id",mViewModel.getUsers_id().getValue());
        bundle.putString("user",mViewModel.getUser().getValue());
        bundle.putString("delivery_day", String.format(Locale.US,"%d-%d-%d", delivery_day.getYear(), delivery_day.getMonth()+1, delivery_day.getDayOfMonth()));
        bundle.putString("delivered",delivered.isChecked() ? "SI":"NO");
        bundle.putString("description",description.getText().toString());

        JSONArray arr = new JSONArray();
        FragmentActivity activity = this.getActivity();
        if(activity instanceof PaqueteDetailActivity){
            PaqueteDetailActivity paqAct = (PaqueteDetailActivity)activity;
            HashSet<String> ids = paqAct.processProductosID();
            HashMap<String,String> quantities = paqAct.getQuantity();

            for (String id : ids) {
                try {
                    JSONObject obj = new JSONObject();
                    obj.put("id",id);
                    obj.put("quantity",quantities.get(id));
                    arr.put(obj);
                } catch (Exception e) {
                    Log.e(TAG,LoginActivity.getStackTrace(e));
                }
            }
        }
        Log.e(TAG, String.format("products_id = %s", arr.toString()));
        bundle.putString("products_id",arr.toString());

        this.waitMessage();
        WS_Paquetes.insert_or_update(this,bundle);
    }
    public static String implode(String glue, String[] strArray)
    {
        String ret = "";
        for(int i=0;i<strArray.length;i++)
        {
            ret += (i == strArray.length - 1) ? strArray[i] : strArray[i] + glue;
        }
        return ret;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // TODO: Use the ViewModel
    }
    @Override
    public void onResume() {

        super.onResume();


        View select_client = root.findViewById(R.id.paquetes_principal_dialog_client);
        View select_gps_device = root.findViewById(R.id.paquetes_principal_dialog_gps_device);
        View select_user= root.findViewById(R.id.paquetes_principal_dialog_user);
        select_client.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PaqueteFragmentPrincipal.this.openSelectClient();
            }
        });
        select_gps_device.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PaqueteFragmentPrincipal.this.openSelectGpsDevice();
            }
        });
        select_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PaqueteFragmentPrincipal.this.openSelectUser();
            }
        });


        delivered.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    String val = _delivered_at;
                    if(val!="" && val!="null"){
                        PaqueteFragmentPrincipal.this.delivered_at.setVisibility(View.VISIBLE);
                    }else{
                        PaqueteFragmentPrincipal.this.delivered_at.setVisibility(View.GONE);
                    }
                } else {
                    // The toggle is disabled
                    PaqueteFragmentPrincipal.this.delivered_at.setVisibility(View.GONE);
                }
            }
        });


        if(_delivered.equals("SI")){
            delivered.setChecked(true);
            delivered_at.setVisibility(View.VISIBLE);
        }else{
            delivered.setChecked(false);
            delivered_at.setVisibility(View.GONE);
        }

    }

    private void openSelectUser() {
        paqueteFragmentSelectUser = new PaqueteFragmentSelectUser();
        paqueteFragmentSelectUser.setPaqueteFragmentPrincipal(this);
        Bundle bundle = new Bundle();
        paqueteFragmentSelectUser.setArguments(bundle);
        paqueteFragmentSelectUser.setCtx(this.ctx);
        paqueteFragmentSelectUser.show(getFragmentManager(), "PaqueteFragmentSelectUser");
    }

    private void openSelectGpsDevice() {
        paqueteFragmentSelectGpsDevice = new PaqueteFragmentSelectGpsDevice();
        paqueteFragmentSelectGpsDevice.setPaqueteFragmentPrincipal(this);
        Bundle bundle = new Bundle();
        paqueteFragmentSelectGpsDevice.setArguments(bundle);
        paqueteFragmentSelectGpsDevice.setCtx(this.ctx);
        paqueteFragmentSelectGpsDevice.show(getFragmentManager(), "PaqueteFragmentSelectGpsDevice");
    }

    private void openSelectClient() {


        paqueteFragmentSelectClient = new PaqueteFragmentSelectClient();
        paqueteFragmentSelectClient.setPaqueteFragmentPrincipal(this);
        Bundle bundle = new Bundle();
        paqueteFragmentSelectClient.setArguments(bundle);
        paqueteFragmentSelectClient.setCtx(this.ctx);
        paqueteFragmentSelectClient.show(getFragmentManager(), "PaqueteFragmentSelectClient");

    }

    public Context getCtx() {
        return ctx;
    }

    public void setCtx(AppCompatActivity ctx) {
        this.ctx = ctx;
    }

    public void setSelectedClient(String client_id, String client) {
        mViewModel.setClient(client);
        mViewModel.setClients_id(client_id);
        if(paqueteFragmentSelectClient!=null){
            paqueteFragmentSelectClient.dismiss();
            paqueteFragmentSelectClient = null;
        }
    }

    private void waitMessage() {
        textview_wait_message.setText(R.string.wait_please);
        textview_wait_message.setVisibility(View.VISIBLE);
    }
    public void hideWaitMessage() {
        textview_wait_message.setVisibility(View.GONE);
    }
    public void queryStatus(String status){
        textview_wait_message.setText(status);
    }

    public void setSelectedGpsDevice(String gps_device_id, String gps_device) {
        mViewModel.setGps_device(gps_device);
        mViewModel.setGps_devices_id(gps_device_id);
        if(paqueteFragmentSelectGpsDevice!=null){
            paqueteFragmentSelectGpsDevice.dismiss();
            paqueteFragmentSelectGpsDevice = null;
        }

    }
    public void setSelectedUser(String user_id, String user) {
        mViewModel.setUser(user);
        mViewModel.setUsers_id(user_id);
        if(paqueteFragmentSelectUser!=null){
            paqueteFragmentSelectUser.dismiss();
            paqueteFragmentSelectUser = null;
        }

    }
}
