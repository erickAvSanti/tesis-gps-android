package com.arquigames.tesis_gps.paquetes.ui.main;

import android.content.Context;
import android.util.AttributeSet;

public class PaqueteFragmentSelectGpsDeviceTextView extends android.support.v7.widget.AppCompatTextView {
    private String gps_device_id;
    private String gps_device;
    public PaqueteFragmentSelectGpsDeviceTextView(Context context) {
        super(context);
    }

    public PaqueteFragmentSelectGpsDeviceTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PaqueteFragmentSelectGpsDeviceTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public String getGps_device_id() {
        return gps_device_id;
    }

    public void setGps_device_id(String gps_device_id) {
        this.gps_device_id = gps_device_id;
    }

    public String getGps_device() {
        return gps_device;
    }

    public void setGps_device(String gps_device) {
        this.gps_device = gps_device;
    }
}
