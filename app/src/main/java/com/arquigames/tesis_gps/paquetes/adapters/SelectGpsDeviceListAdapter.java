package com.arquigames.tesis_gps.paquetes.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.arquigames.tesis_gps.LoginActivity;
import com.arquigames.tesis_gps.R;
import com.arquigames.tesis_gps.paquetes.PaqueteDetailActivity;
import com.arquigames.tesis_gps.paquetes.ui.main.PaqueteFragmentSelectGpsDevice;
import com.arquigames.tesis_gps.paquetes.ui.main.PaqueteFragmentSelectGpsDeviceTextView;
import com.arquigames.tesis_gps.paquetes.ui.main.PaqueteFragmentSelectGpsDeviceTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

public class SelectGpsDeviceListAdapter extends ArrayAdapter<JSONObject> {
    private static final String TAG = "SelGpsDevListAdapter";
    private PaqueteFragmentSelectGpsDevice ctx;
    private int resource;
    private HashMap<String, JSONObject> mapObjects = new HashMap<>();

    public SelectGpsDeviceListAdapter(PaqueteFragmentSelectGpsDevice context, int textViewResourceId,
                                      List<JSONObject> objects) {
        super(context.getCtx(), textViewResourceId, objects);
        ctx = context;
        resource = textViewResourceId;
        for (JSONObject obj : objects) {
            try {
                String id = obj.getString("id");
                Log.e("Init ID", id);
                mapObjects.put(id, obj);
            } catch (JSONException e) {
                Log.e(TAG, LoginActivity.getStackTrace(e));
            } catch (Exception e) {
                Log.e(TAG, LoginActivity.getStackTrace(e));
            }
        }
    }

    @Override
    public long getItemId(int position) {
        JSONObject item = getItem(position);
        int id = 0;
        try {
            id = Integer.parseInt(item.getString("id"));
        } catch (JSONException e) {
            Log.e(TAG,LoginActivity.getStackTrace(e));
        }catch (Exception e){
            Log.e(TAG,LoginActivity.getStackTrace(e));
        }
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(ctx.getCtx());
            v = vi.inflate(resource, null);
        }

        JSONObject data = getItem(position);

        if (data != null) {
            PaqueteFragmentSelectGpsDeviceTextView name = v.findViewById(R.id.paquetes_select_gps_device_list_item_name);
            try {
                name.setText(String.format("%s %s - %s", data.getString("brand"), data.getString("model"), data.getString("phone_number")));
                name.setGps_device_id(data.getString("id"));
                name.setGps_device(String.format("%s %s / %s", data.getString("brand"), data.getString("model"), data.getString("phone_number")));
                name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PaqueteFragmentSelectGpsDeviceTextView tt = (PaqueteFragmentSelectGpsDeviceTextView)v;
                        ctx.getPaqueteFragmentPrincipal().setSelectedGpsDevice(tt.getGps_device_id(),tt.getGps_device());
                    }
                });
            } catch (JSONException e) {
                Log.e(TAG,LoginActivity.getStackTrace(e));
                if(name!=null){
                    name.setText("Error");
                }
            }
            catch (Exception e){
                Log.e(TAG,LoginActivity.getStackTrace(e));
            }
        }else{
            Log.e(TAG, String.format("Data is null at %d", position));
        }

        return v;
    }


    @Override
    public boolean hasStableIds() {
        return true;
    }
}
