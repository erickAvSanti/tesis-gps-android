package com.arquigames.tesis_gps.paquetes;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import com.arquigames.tesis_gps.LoginActivity;
import com.arquigames.tesis_gps.R;
import com.arquigames.tesis_gps.paquetes.ui.main.PaqueteFragmentProductos;
import com.arquigames.tesis_gps.paquetes.ui.main.SectionsPagerAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class PaqueteDetailActivity extends AppCompatActivity implements PaqueteFragmentProductos.OnPaqueteFragmentProductosInteractionListener {


    private static final String TAG = "PaDetailActivity";
    private HashSet<String> productosToRemove   = new HashSet<>();
    private HashSet<String> productosID         = new HashSet<>();
    private HashMap<String, String> quantity    = new HashMap();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent extras = this.getIntent();
        setContentView(R.layout.activity_paquete_detail);
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager(),extras);
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            NavUtils.navigateUpTo(this, new Intent(this, PaquetesActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onResume() {

        super.onResume();
    }

    @Override
    public void removeProducto(String id) {
        productosToRemove.add(id);
    }
    @Override
    public void clearRemoveProducto() {
        productosToRemove.clear();
    }

    @Override
    public void setProductos(List<JSONObject> values, HashMap<String, String> quantity) {
        this.clearRemoveProducto();
        productosID.clear();
        this.quantity = quantity;
        for (JSONObject id : values) {
            try {
                productosID.add(id.getString("id"));
            } catch (JSONException e) {
                Log.e(TAG, LoginActivity.getStackTrace(e));
            }
        }
    }

    public HashSet<String> processProductosID(){
        for (String s : productosID) {
            if (productosToRemove.contains(s)) {
                productosID.remove(s);
                quantity.remove(s);
            }
        }
        return productosID;
    }
    public HashMap<String,String> getQuantity(){
        return quantity;
    }
}