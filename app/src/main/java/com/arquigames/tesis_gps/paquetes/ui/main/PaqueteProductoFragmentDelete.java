package com.arquigames.tesis_gps.paquetes.ui.main;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.arquigames.tesis_gps.R;
import com.arquigames.tesis_gps.webservices.WS_Productos;

public class PaqueteProductoFragmentDelete extends AppCompatDialogFragment {
    private AppCompatTextView name;
    private AppCompatButton btn_delete;
    private AppCompatButton btn_cancel;
    private TextView textview_wait_message;
    private Bundle bundle;
    private String id;
    private PaqueteFragmentProductos paqueteFragmentProductos;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        android.support.v7.app.AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.dialog_fragment_paquetes_productos_delete, null));
        return builder.create();
    }
    @Override
    public void onResume() {

        super.onResume();
        bundle = getArguments();
        name                    = this.getDialog().findViewById(R.id.paquetes_productos_dialog_name);
        btn_delete              = this.getDialog().findViewById(R.id.paquetes_productos_dialog_delete);
        btn_cancel              = this.getDialog().findViewById(R.id.paquetes_productos_dialog_cancel);
        textview_wait_message   = this.getDialog().findViewById(R.id.paquetes_productos_dialog_delete_wait_message);
        name.setText(String.format("Desea retirar el producto %s ?", bundle.getString("name")));
        id = bundle.getString("id");
        this.hideWaitMessage();

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PaqueteProductoFragmentDelete.this.dismiss();
            }
        });
        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO
                if(paqueteFragmentProductos!=null){
                    paqueteFragmentProductos.removeProducto(id);
                }
                PaqueteProductoFragmentDelete.this.dismiss();
            }
        });
    }


    private void waitMessage() {
        textview_wait_message.setText(R.string.wait_please);
        textview_wait_message.setVisibility(View.VISIBLE);
    }
    private void hideWaitMessage() {
        textview_wait_message.setVisibility(View.GONE);
    }

    public void queryStatus(String status) {
        textview_wait_message.setText(status);
    }

    public void setPaqueteFragmentProductos(PaqueteFragmentProductos paqueteFragmentProductos) {
        this.paqueteFragmentProductos = paqueteFragmentProductos;
    }
}
