package com.arquigames.tesis_gps.paquetes;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.arquigames.tesis_gps.AppActivity;
import com.arquigames.tesis_gps.LoginActivity;
import com.arquigames.tesis_gps.R;
import com.arquigames.tesis_gps.paquetes.adapters.PaquetesListAdapter;
import com.arquigames.tesis_gps.webservices.WS_Paquetes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class PaquetesActivity extends AppCompatActivity {

    private Handler handler = new Handler();
    private ArrayList<String> list;
    private HashMap<String,JSONObject> list_map;
    private PaquetesListAdapter adapter;
    private ListView swipe_list;
    private SwipeRefreshLayout swipe;
    public static String TAG = "PaquetesActivity";
    public static String KEY_SHARED_PREFERENCE  ="paquetes_result";

    private int pagination = 0;
    private int page = 1;
    private String search = "";
    private String order_by_field = "";
    private String order_by_cond = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paquetes);


        Intent intent_extras = this.getIntent();
        String source = "";
        if(intent_extras!=null){
            source = intent_extras.getStringExtra("result");
            if(source!="" && source!=null){
                LoginActivity.putSharedStringValue(this,KEY_SHARED_PREFERENCE,source);
            }else{
                source = LoginActivity.getSharedStringValue(this,KEY_SHARED_PREFERENCE);
            }
        }else{
            source = LoginActivity.getSharedStringValue(this,KEY_SHARED_PREFERENCE);
        }
        if(source!=null)
            source = source.trim();
        else
            source="";

        JSONArray arr = new JSONArray();
        try {
            if(!source.isEmpty()){
                Log.e(TAG,"SOURCE => "+source);
                arr = new JSONArray(source);
                page = 1;
                pagination = 20;
            }
        } catch (JSONException e) {
            Log.e(TAG,LoginActivity.getStackTrace(e));
        }


        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        swipe = findViewById(R.id.swipe_paquetes);
        swipe_list = findViewById(R.id.swipe_paquetes_list);

        swipe.setColorSchemeResources(R.color.color_swipe_list_foreground);
        swipe.setProgressBackgroundColorSchemeResource(R.color.color_swipe_list_background);

        if(source.isEmpty()){
            this.getList();
        }
        SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                PaquetesActivity.this.getList();
            }
        };
        swipe.setOnRefreshListener(onRefreshListener);
        this.fillList(arr);
    }

    public void getList() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                PaquetesActivity.this.getData();
            }
        });
    }

    public void fillList(JSONArray arr){
        List<JSONObject> values = new ArrayList<>();
        if(list_map!=null){
            list_map.clear();
        }else{
            list_map = new HashMap<>();
        }
        if(arr.length()>0){
            int pag = (page-1)*pagination;
            for(int i=0;i<arr.length();i++){
                try {
                    JSONObject obj = arr.getJSONObject(i);
                    obj = AppActivity.cleanJSONObject(obj);
                    values.add(obj);
                    list_map.put(obj.getString("id"),obj);
                } catch (JSONException e) {
                    Log.e(TAG,LoginActivity.getStackTrace(e));
                }
            }
        }
        adapter = new PaquetesListAdapter(this, R.layout.paquetes_list_item, values);
        swipe_list.setAdapter(adapter);
        notifySwipeAdapter();
    }
    private void getData(){
        WS_Paquetes.reload(PaquetesActivity.this,page,search,order_by_field,order_by_cond);
    }
    public void stopSwipeRefreshing(){
        swipe.setRefreshing(false);
    }
    public void notifySwipeAdapter(){
        stopSwipeRefreshing();
        adapter.notifyDataSetChanged();
    }

    public void setPage(int page) {
        this.page = page;
    }
    public void previousPage(View view){
        this.page--;
        if(page<1)page=1;
        if(!swipe.isRefreshing())swipe.setRefreshing(true);
        getData();
    }
    public void nextPage(View view){
        this.page++;
        if(!swipe.isRefreshing())swipe.setRefreshing(true);
        getData();
    }
    public void createPaquete(View view){
        Intent intent = new Intent(PaquetesActivity.this, com.arquigames.tesis_gps.paquetes.PaqueteDetailActivity.class);
        startActivity(intent);
    }
    public void editPaquete(JSONObject info){
        Intent intent = new Intent(PaquetesActivity.this, com.arquigames.tesis_gps.paquetes.PaqueteDetailActivity.class);

        Bundle bundle = this.getBundlePaquete(info);

        for (String key : bundle.keySet()) {
            intent.putExtra(key, bundle.getString(key));
        }
        startActivity(intent);
    }
    public void setPagination(int pag) {
        pagination = pag;
    }

    public void editPaquete(String id) {
        this.editPaquete(list_map.get(id));
    }
    private Bundle getBundlePaquete(JSONObject info){
        JSONObject extra;
        Object tmp;
        Bundle bundle = new Bundle();
        try {
            bundle.putString("name",info.getString("name"));
            bundle.putString("clients_id",info.getString("clients_id"));
            bundle.putString("gps_devices_id",info.getString("gps_devices_id"));
            bundle.putString("users_id",info.getString("users_id"));
            bundle.putString("delivery_day",info.getString("delivery_day"));
            bundle.putString("delivered",info.getString("delivered"));
            bundle.putString("delivered_at",info.getString("delivered_at"));
            bundle.putString("description",info.getString("description"));
            bundle.putString("id",info.getString("id"));


            if(info.has("client")){
                tmp = info.get("client");
                if(tmp instanceof JSONObject){
                    extra = (JSONObject) tmp;
                }else{
                    extra = null;
                }
                if(
                        extra != null &&
                                extra.has("firstname") &&
                                extra.has("lastname")
                ){
                    bundle.putString("client", String.format("%s %s", extra.getString("firstname"), extra.getString("lastname")));
                }else{
                    bundle.putString("client","");
                }
            }else{
                bundle.putString("client","");
            }
            if(info.has("user")){
                tmp = info.get("user");
                if(tmp instanceof JSONObject){
                    extra = (JSONObject) tmp;
                }else{
                    extra = null;
                }
                if(
                        extra != null &&
                                extra.has("name")
                ){
                    bundle.putString("user", String.format("%s", extra.getString("name")));
                }else{
                    bundle.putString("user","");
                }
            }else{
                bundle.putString("user","");
            }
            if(info.has("gps_device")){
                tmp = info.get("gps_device");
                if(tmp instanceof JSONObject){
                    extra = (JSONObject) tmp;
                }else{
                    extra = null;
                }
                if(
                        extra != null &&
                                extra.has("brand")
                ){
                    bundle.putString("gps_device", String.format("%s %s / %s", extra.getString("brand"), extra.getString("model"), extra.getString("phone_number")));
                }else{
                    bundle.putString("gps_device","");
                }
            }else{
                bundle.putString("gps_device","");
            }

        } catch (JSONException e) {
            Log.e(TAG,LoginActivity.getStackTrace(e));
        }
        return bundle;
    }

    public void deletePaquete(String producto_id) {
        JSONObject productoInfo = list_map.get(producto_id);
        Bundle bundle = this.getBundlePaquete(productoInfo);
        if(bundle.size()>0){
            PaqueteFragmentDelete dialog = new PaqueteFragmentDelete();
            dialog.setArguments(bundle);
            dialog.show(getSupportFragmentManager(), "PaqueteFragmentDelete");
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            NavUtils.navigateUpTo(this, new Intent(this, AppActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
