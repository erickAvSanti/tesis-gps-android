package com.arquigames.tesis_gps.paquetes.ui.main;

import android.content.Context;
import android.util.AttributeSet;

public class PaqueteFragmentSelectUserTextView extends android.support.v7.widget.AppCompatTextView {
    private String user_id;
    private String user;
    public PaqueteFragmentSelectUserTextView(Context context) {
        super(context);
    }

    public PaqueteFragmentSelectUserTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PaqueteFragmentSelectUserTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
